//
//  UIButton+Extensions.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 30/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

extension UIButton {
    public func addShowAnimation(delay: Double) {
        UIView.animate(withDuration: 0.25, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
}
