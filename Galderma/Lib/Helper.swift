//
//  Helper.swift
//  ising
//
//  Created by Ufos on 26.05.2017.
//  Copyright © 2017 Panowie-Programisci. All rights reserved.
//

import Foundation
import UIKit

//

func isIPAD () -> Bool { return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad }
func isIPhone() -> Bool { return !isIPAD() }

//

func log(_ string: String) {
    NSLog(string)
}

// time in seconds
func delay(_ time: TimeInterval, withCompletion completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time, execute: { () -> Void in
        completion()
    })
}

// safe [0] checking
extension Array {
    subscript (safe index: Int) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

//

func stringForKey(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

//

extension CGRect {
    func resize(_ tw: CGFloat, th: CGFloat) -> CGRect{
        return CGRect(x: self.origin.x, y: self.origin.y, width: self.size.width + tw, height: self.size.height + th)
    }
    
    func rectWithHeight(_ height: CGFloat) -> CGRect{
        return CGRect(x: self.origin.x, y: self.origin.y, width: self.size.width, height: height)
    }
    
    func move(_ dx: CGFloat, dy: CGFloat) -> CGRect{
        return CGRect(x: self.origin.x + dx, y: self.origin.y + dy, width: self.size.width, height: self.size.height)
    }
    
    func setX(_ x: CGFloat) -> CGRect{
        return CGRect(x: x, y: self.origin.y, width: self.size.width, height: self.size.height)
    }
}

//

extension UIImage {
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }

    static func createCircle(color: UIColor, radius: CGFloat) -> UIImage? {
        let image = UIImage(color: color)!
        
        let width = radius*2
        let scale = width / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: width, height: newHeight))
        
        UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: width, height: newHeight)).addClip()
        
        image.draw(in: CGRect(x: 0, y: 0, width: width, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}


//

extension UISearchBar {
    
    // I cannot understand why it's not automatically built in
    func setCancelText(text: String) {
        self.cancelButton?.setTitle(text, for: .normal)
    }
    
    fileprivate var cancelButton: UIButton? {
        
        for sv in self.subviews {
            if let sv_ = sv as? UIButton {
                return sv_
            }
            
            for chilsv in sv.subviews {
                if let sv_ = chilsv as? UIButton {
                    return sv_
                }
            }
        }
        
        return nil
    }
    
}

//


extension Int {

    var timeString: String {
        let m = self / 60
        let s = self % 60
        
        return "\(m):" + ((s < 10) ? "0" : "") + "\(s)"
    }
}

//

var statusBarView: UIView? {
    return UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
}

func setStatusBarBackgroundColor(color: UIColor? = nil) {
    guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
    
    statusBar.backgroundColor = color
}

//

func stringToDate(_ string: String) -> Date {
    let dateFormatter = ISO8601DateFormatter()
    let date = Date()
    
    if let dateFromString = dateFormatter.date(from: string) {
        return dateFromString
    }
    
    return date
}

//

func sizeForLocalFilePath(filePath: String) -> UInt64 {
    do {
        let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
        if let fileSize = fileAttributes[FileAttributeKey.size]  {
            return (fileSize as! NSNumber).uint64Value
        } else {
            print("Failed to get a size attribute from path: \(filePath)")
        }
    } catch {
        print("Failed to get file attributes for local path: \(filePath) with error: \(error)")
    }
    return 0
}

//

extension UIButton {
    func addShadow(heightOffset: CGFloat = 15.0, radius: CGFloat = 15.0) {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: heightOffset)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
}

// https://stackoverflow.com/questions/40991450/ios-present-uialertcontroller-on-top-of-everything-regardless-of-the-view-hier

public extension UIViewController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}

// https://stackoverflow.com/questions/21857222/how-to-push-two-view-controllers-but-animate-transition-only-for-the-second-one

extension UINavigationController {
    open func pushViewControllers(_ inViewControllers: [UIViewController], animated: Bool) {
        var stack = self.viewControllers
        stack.append(contentsOf: inViewControllers)
        self.setViewControllers(stack, animated: animated)
    }
}

// https://stackoverflow.com/questions/39768532/how-do-i-prevent-repeated-button-presses-in-ios

extension UIButton {
    func preventRepeatedPresses(inNext seconds: Double = 1) {
        self.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            self.isUserInteractionEnabled = true
        }
    }
}
