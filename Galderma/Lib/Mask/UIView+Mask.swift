//
//  UIView+Mask.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 19/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

// Mask UIView with rect/path
// inverse = false: part inside rect is kept, everything outside is clipped
// inverse = true: part inside rect is cut out, everything else is kept
//
// Usage:
//
// // - Cuts rectangle inside view, leaving 20pt borders around
// let rect = CGRect(x: 20, y: 20, width: bounds.size.width - 20*2, height: bounds.size.height - 20*2)
// targetView.mask(withRect: rect, inverse: true)
//

import UIKit

extension UIView {
    func mask(withRect rect: CGRect, inverse: Bool = false) {
        let path = UIBezierPath(rect: rect)
        let maskLayer = CAShapeLayer()
        
        if inverse {
            path.append(UIBezierPath(rect: self.bounds))
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        }
        
        maskLayer.path = path.cgPath
        
        self.layer.mask = maskLayer
    }
    
    func mask(withPath path: UIBezierPath, inverse: Bool = false) {
        let path = path
        let maskLayer = CAShapeLayer()
        
        if inverse {
            path.append(UIBezierPath(rect: self.bounds))
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        }
        
        maskLayer.path = path.cgPath
        
        self.layer.mask = maskLayer
    }
}
