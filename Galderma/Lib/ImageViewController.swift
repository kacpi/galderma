//
//  ImageViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 11/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit
// import MBProgressHUD

class ImageViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var image: UIImage?
    
    private var imageView: UIImageView!
    
    var didClose: (()->())?
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
//        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        imageView = UIImageView()
        
        guard let image_ = self.image else { return }
        
//        imageView.image = image_
        
        self.configureSizeAndZoomScale(image: image_)
        
//        imageView.sd_setImageWithURL(NSURL(string: imageURL!), completed: { [weak self] (image, error, cacheType, url) -> Void in
//            self?.configureSizeAndZoomScale(image)
//            MBProgressHUD.hideHUDForView(self?.view, animated: true)
//        })
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { context in self.centerScrollViewContents() }, completion: nil)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.didClose?()
    }
    
    @IBAction func scrollViewDoubleTapped(recognizer: UITapGestureRecognizer) {
        let pointInView = recognizer.location(in: imageView)
        if scrollView.zoomScale < scrollView.maximumZoomScale {
            zoomToScale(scale: scrollView.maximumZoomScale, pointInView: pointInView)
        } else {
            zoomToScale(scale: scrollView.minimumZoomScale, pointInView: pointInView)
        }
    }
    
    func configureSizeAndZoomScale(image: UIImage?) {
        if let image = image {
            imageView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size:image.size)
            imageView.image = image
            scrollView.addSubview(imageView)
            scrollView.contentSize = image.size
            
            let scrollViewFrame = scrollView.frame
            let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
            let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
            let minScale = min(scaleWidth, scaleHeight)
            
            scrollView.minimumZoomScale = minScale
            scrollView.maximumZoomScale = 15.0
            scrollView.zoomScale = minScale
            
            centerScrollViewContents()
        }
    }
    
    func centerScrollViewContents() {
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        
        imageView.frame = contentsFrame
    }
    
    func zoomToScale(scale: CGFloat, pointInView: CGPoint) {
        let scrollViewSize = scrollView.bounds.size
        let w = scrollViewSize.width / scale
        let h = scrollViewSize.height / scale
        let x = pointInView.x - (w / 2.0)
        let y = pointInView.y - (h / 2.0)
        
        let rectToZoomTo = CGRect(x: x, y: y, width: w, height: h)
        
        scrollView.zoom(to: rectToZoomTo, animated: true)
    }
    
    
    //MARK: - UIScrollViewDelegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerScrollViewContents()
    }

}
