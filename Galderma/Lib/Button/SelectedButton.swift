//
//  SelectedButton.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SelectedButton: UIButton {
    
    override var isSelected: Bool {
        willSet {
            self.backgroundColor = UIColor.clear
        }
        
        didSet {
            self.backgroundColor = UIColor.clear
        }
    }
    
}
