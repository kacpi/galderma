//
//  ShadowButton.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 15/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class ShadowButton: UIButton {
    /*
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.addShadow()
    }
    */
    
    override func awakeFromNib() {
        self.addShadow()
    }
}
