//
//  TabBarView.swift
//  MakeApp
//
//  Created by ufos on 05.02.2016.
//  Copyright © 2016 makeapp. All rights reserved.
//

import Foundation
import UIKit

class TabBarView: UIScrollView {
    
    let TABBAR_UNDERLINE_HEIGHT: CGFloat = 4
    let TABBAR_LINE_HEIGHT: CGFloat = 1
    @IBInspectable var TABBAR_BUTTON_MIN_WIDTH: CGFloat = 100
    
    
    private(set) var currentPageIndex: Int = 0
    var tabButtons: [UIButton]!
    private var underlineView: UIView!
    private var lineView: UIView!
    
    private(set) var buttonWidth: CGFloat = 0
    
    // different approach for PageViewController
    var isPageVc: Bool = false
    
    //

    
    var notSelectedColor: UIColor = UIColor.white.withAlphaComponent(0.6) {
        didSet {
            for button in self.tabButtons {
                button.setTitleColor(self.notSelectedColor, for: UIControl.State.normal)
            }
        }
    }
    
    var underlineColor: UIColor! = UIColor.black {
        didSet {
            self.underlineView?.backgroundColor = self.underlineColor
        }
    }
    
    private var underlineColor_: UIColor! {
        get {
            if self.underlineColor == nil {
                return self.tintColor
            }
            return self.underlineColor
        }
    }
    
    //
    
    weak var tabBarDelegate: TabBarDelegate?
    
    var buttonFont: UIFont = UIFont(name: "OpenSans-Semibold", size: 15)! {
        didSet {
            for button in self.tabButtons {
                button.titleLabel?.font = self.buttonFont
            }
        }
    }
    
    var buttonSelectedFont: UIFont = UIFont(name: "OpenSans-Semibold", size: 15)!
    
    //
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        self.calculateButtonsWidth()
        
        self.delaysContentTouches = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.tabButtons = []
        self.clipsToBounds = true
        
        self.delaysContentTouches = false
    }
    
    
    // FOR USER
    
    func selectTab(index: Int, animated: Bool = true) {
        self.currentPageIndex = index
        self.setButtonSelected(buttonIndex: self.currentPageIndex)
        let underlinePos = CGFloat(self.currentPageIndex*Int(self.buttonWidth))
        if (animated) {
            UIView.animate(withDuration: 0.3) { () -> Void in
                self.underlineView.frame = self.underlineView.frame.setX(underlinePos)
                // to make underline always visible
                self.contentOffset.x = min(max(underlinePos - self.frame.width/3, 0), self.contentSize.width - self.frame.width)
            }
        } else {
            self.underlineView.frame = self.underlineView.frame.setX(underlinePos)
            // to make underline always visible
            self.contentOffset.x = min(max(underlinePos - self.frame.width/3, 0), self.contentSize.width - self.frame.width)
        }
    }
    
    func alignToCenter() {
        let viewWidth =  self.frame.width
        let contentWidth = self.buttonWidth * CGFloat(self.tabButtons.count)
        
        self.contentOffset = CGPoint(x: (max(0, contentWidth - viewWidth)) / 2, y: 0)
    }
    
    //
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.calculateButtonsWidth()
    }
    
    // when content is scrolling
    func contentDidScroll(scrollPos: CGFloat, scrollWidth: CGFloat) {
        
        // calculate underlineView x-pos
        let diff = scrollPos - scrollWidth
        var underlinePos: CGFloat = 0
        
        if(self.isPageVc == true) {
            underlinePos = CGFloat(self.currentPageIndex) * self.buttonWidth + diff * (self.buttonWidth/scrollWidth)
            
            // if mnore than half scrolled -> setButtonSelected
            let diffDiv = diff/scrollWidth
            
            if(diffDiv > 0) {
                if(diffDiv > 0.5 && self.tabButtons.count > self.currentPageIndex+1) {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex+1)
                } else {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex)
                }
            } else {
                if(diffDiv < -0.5 && self.currentPageIndex > 0) {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex-1)
                } else {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex)
                }
            }
        }
            // if PageVC then scrollPos is calculated from current tabs pos
        else {
            underlinePos = self.buttonWidth + diff * (self.buttonWidth/scrollWidth)
            self.currentPageIndex = min(self.tabButtons.count-1, Int((scrollPos+(scrollWidth/2)) / scrollWidth))
            self.setButtonSelected(buttonIndex: self.currentPageIndex)
        }
        
        self.underlineView.frame = self.underlineView.frame.setX(underlinePos)
    }
    
    // if possible, align all buttons, else use min width
    func calculateButtonsWidth() {
        if(self.tabButtons != nil && self.tabButtons.count > 0) {
            let fitWidth = self.frame.width / CGFloat(self.tabButtons.count)
            self.buttonWidth = max(TABBAR_BUTTON_MIN_WIDTH, fitWidth)
            
            for i in 0 ..< self.tabButtons.count {
                self.tabButtons[i].frame = CGRect(x: CGFloat(i*Int(self.buttonWidth)), y: 0, width: self.buttonWidth, height: self.frame.height)
            }
            self.contentSize = CGSize(width: CGFloat(self.tabButtons.count*Int(self.buttonWidth)), height: self.frame.height)
            
            self.setButtonSelected(buttonIndex: self.currentPageIndex)
            self.underlineView.frame = CGRect(x: CGFloat(self.currentPageIndex*Int(self.buttonWidth)), y: self.frame.height-TABBAR_UNDERLINE_HEIGHT, width: self.buttonWidth, height: TABBAR_UNDERLINE_HEIGHT)
            
            self.lineView.frame = CGRect(x: 0, y: self.bounds.height - TABBAR_LINE_HEIGHT, width: self.bounds.width, height: TABBAR_LINE_HEIGHT)
        }
    }
    
    // if not called there is no tabbar
    func configureTabBar(titles: [String], titleBottomInset: CGFloat = 0, titleTopInset: CGFloat = 0) {
        // configure TabBar
        self.bounces = false
        self.isScrollEnabled = true
        self.alwaysBounceHorizontal = true
        self.showsHorizontalScrollIndicator = false
        
        if(self.lineView == nil) {
            self.lineView = UIView()
//            self.lineView.backgroundColor = UIColor.init(hex: "e8e8e8")
            self.lineView.backgroundColor = UIColor.red
            
            self.addSubview(self.lineView)
        }
        
        if(self.underlineView == nil) {
            self.underlineView = UIView()
            self.underlineView.backgroundColor = self.underlineColor_
            self.addSubview(self.underlineView)
        }
        
        self.tabButtons = []
        
        for i in 0 ..< titles.count {
            let button = UIButton(type: UIButton.ButtonType.system)
            button.frame = CGRect(x: CGFloat(i*Int(self.buttonWidth)), y: 0, width: self.buttonWidth, height: self.frame.height)
            button.setTitle(titles[i], for: UIControl.State.normal)
            button.setTitleColor(self.tintColor, for: UIControl.State.normal)
//            button.setTitleColor(self.notSelectedColor.withAlphaComponent(0.3), for: .highlighted)
            button.titleLabel?.font = self.buttonFont
            button.tag = i
            button.titleEdgeInsets = UIEdgeInsets(top: titleTopInset, left: 0, bottom: titleBottomInset, right: 0)
            button.addTarget(self, action: #selector(tabButtonClicked), for: UIControl.Event.touchUpInside)
            self.addSubview(button)
            self.tabButtons.append(button)
        }
        
        self.calculateButtonsWidth()
    }
    
    @objc func tabButtonClicked(button: UIButton) {
        self.selectTab(index: button.tag)
        self.tabBarDelegate?.tabClicked(index: button.tag)
    }
    
    private func setButtonSelected(buttonIndex: Int) {
        for button in self.tabButtons {
            button.setTitleColor(self.notSelectedColor, for: .normal)
//            button.setTitleColor(self.notSelectedColor.withAlphaComponent(0.3), for: .highlighted)
            
            button.titleLabel?.font = self.buttonFont
        }

        self.tabButtons[buttonIndex].setTitleColor(self.tintColor, for: .normal)
//        self.tabButtons[buttonIndex].setTitleColor(self.tintColor.withAlphaComponent(0.3), for: .highlighted)
        self.tabButtons[buttonIndex].titleLabel?.font = self.buttonSelectedFont
    }
}


//

protocol TabBarDelegate: class {
    func tabClicked(index: Int)
}


