//
//  ChplViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class ChplViewController: ViewController {
    
    @IBOutlet weak var button1: ShadowButton!
    @IBOutlet weak var button2: ShadowButton!
    @IBOutlet weak var button3: ShadowButton!
    @IBOutlet weak var button4: ShadowButton!
    @IBOutlet weak var button5: ShadowButton!
    
    fileprivate let DELAY: TimeInterval = 0.25
    fileprivate var isClicked: Bool = false
    
    fileprivate var subscribers = [Any]()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        button1.alpha = 0.0
        button2.alpha = 0.0
        button3.alpha = 0.0
        button4.alpha = 0.0
        button5.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.button1.addShowAnimation(delay: 0.1)
        self.button2.addShowAnimation(delay: 0.2)
        self.button3.addShowAnimation(delay: 0.3)
        self.button4.addShowAnimation(delay: 0.4)
        self.button5.addShowAnimation(delay: 0.5)
    }
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    //
    
    func showPdfVc(name: String, logo: UIImage, visibleButtons: Bool = false) {
        self.performSegue("showPdfVc") { (destVc) in
            (destVc as! PdfViewController).pdfName = name
            (destVc as! PdfViewController).logoImage = logo
            (destVc as! PdfViewController).isStackViewVisible = visibleButtons
        }
    }
    
    func animate(button: UIButton, imageButton: ChplNormal, imageButtonSelected: ChplSelected, logo: PdfLogo, visibleButtons: Bool = false) {
        guard !self.isClicked else { return }
        
        self.isClicked = true
        
        button.setImage(UIImage(named: imageButtonSelected.rawValue), for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + DELAY) {
            if let logo_ = UIImage(named: logo.rawValue) {
                switch imageButton {
                case .EPIDUO:
                    self.showPdfVc(name: "pdf_epiduo", logo: logo_, visibleButtons: visibleButtons)
                case .EPIDUO_FORTE:
                    self.showPdfVc(name: "pdf_epiduo_forte", logo: logo_, visibleButtons: visibleButtons)
                case .SOOLANTRA:
                    self.showPdfVc(name: "pdf_soolantra", logo: logo_, visibleButtons: visibleButtons)
                case .TETRA:
                    self.showPdfVc(name: "pdf_tetralysal_150", logo: logo_, visibleButtons: visibleButtons)
                case .ROZEX:
                    self.showPdfVc(name: "pdf_rozex", logo: logo_, visibleButtons: visibleButtons)
                }
                
                button.setImage(UIImage(named: imageButton.rawValue), for: .normal)
            }
            
            self.isClicked = false
        }
    }
    
}

// show vc

extension ChplViewController {
    func configureNavigation() {
        subscribers.append(EventSubscriber.subscribe(tag: "showChplEpiduoVc", callback: { [weak self] (_) in
            if let logo_ = UIImage(named: PdfLogo.EPIDUO.rawValue) {
                self?.showPdfVc(name: "pdf_epiduo", logo: logo_, visibleButtons: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showChplForteVc", callback: { [weak self] (_) in
            if let logo_ = UIImage(named: PdfLogo.EPIDUO_FORTE.rawValue) {
                self?.showPdfVc(name: "pdf_epiduo_forte", logo: logo_, visibleButtons: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showChplSoolantraVc", callback: { [weak self] (_) in
            if let logo_ = UIImage(named: PdfLogo.SOOLANTRA.rawValue) {
                self?.showPdfVc(name: "pdf_soolantra", logo: logo_, visibleButtons: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showChplTetralysalVc", callback: { [weak self] (_) in
            if let logo_ = UIImage(named: PdfLogo.TETRA.rawValue) {
                self?.showPdfVc(name: "pdf_tetralysal_150", logo: logo_, visibleButtons: true)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showChplRozexVc", callback: { [weak self] (_) in
            if let logo_ = UIImage(named: PdfLogo.ROZEX.rawValue) {
                self?.showPdfVc(name: "pdf_rozex", logo: logo_, visibleButtons: false)
            }
        }))
    }
}

// IBActions

extension ChplViewController {
    @IBAction func epiduoButtonClicked(_ sender: Any) {
        if let button = sender as? UIButton {
            self.animate(button: button, imageButton: .EPIDUO, imageButtonSelected: .EPIDUO, logo: .EPIDUO)
        }
    }
    
    @IBAction func epiduoForteButtonClicked(_ sender: Any) {
        if let button = sender as? UIButton {
            self.animate(button: button, imageButton: .EPIDUO_FORTE, imageButtonSelected: .EPIDUO_FORTE, logo: .EPIDUO_FORTE)
        }
    }
    
    @IBAction func soolantraButtonClicked(_ sender: Any) {
        if let button = sender as? UIButton {
            self.animate(button: button, imageButton: .SOOLANTRA, imageButtonSelected: .SOOLANTRA, logo: .SOOLANTRA)
        }
    }
    
    @IBAction func tetralysalButtonClicked(_ sender: Any) {
        if let button = sender as? UIButton {
            self.animate(button: button, imageButton: .TETRA, imageButtonSelected: .TETRA, logo: .TETRA, visibleButtons: true)
        }
    }
    
    @IBAction func rozexButtonClicked(_ sender: Any) {
        if let button = sender as? UIButton {
            self.animate(button: button, imageButton: .ROZEX, imageButtonSelected: .ROZEX, logo: .ROZEX)
        }
    }
}

//

enum PdfLogo: String {
    case EPIDUO = "pdf_1_epiduo_logo"
    case EPIDUO_FORTE = "pdf_2_epiduo_forte_logo"
    case SOOLANTRA = "pdf_3_soolantra_logo"
    case TETRA = "pdf_4_tetralysal_logo"
    case ROZEX = "pdf_5_rozex_logo"
}

enum ChplNormal: String {
    case EPIDUO = "chpl_1_epiduo"
    case EPIDUO_FORTE = "chpl_2_epiduo_forte"
    case SOOLANTRA = "chpl_3_soolantra"
    case TETRA = "chpl_4_tetralysal"
    case ROZEX = "chpl_5_rozex"
}

enum ChplSelected: String {
    case EPIDUO = "chpl_1_epiduo_selected"
    case EPIDUO_FORTE = "chpl_2_epiduo_forte_selected"
    case SOOLANTRA = "chpl_3_soolantra_selected"
    case TETRA = "chpl_4_tetralysal_selected"
    case ROZEX = "chpl_5_rozex_selected"
}
