//
//  MainEpiduoViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 30/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class MainEpiduoViewController: ViewController {
    
    @IBOutlet weak var epiduoButton: ShadowButton!
    @IBOutlet weak var forteButton: ShadowButton!
    
    //
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.epiduoButton.alpha = 0.0
        self.forteButton.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.epiduoButton.addShowAnimation(delay: 0.1)
        self.forteButton.addShowAnimation(delay: 0.2)
    }
    
}
