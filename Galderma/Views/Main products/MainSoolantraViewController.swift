//
//  MainSoolantraViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 30/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class MainSoolantraViewController: ViewController {
    
    @IBOutlet weak var soolantraButton: ShadowButton!
    
    //
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.soolantraButton.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.soolantraButton.addShowAnimation(delay: 0.1)
    }
    
}
