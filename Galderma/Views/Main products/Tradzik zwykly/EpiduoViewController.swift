//
//  EpiduoViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoViewController: ViewController {
    
    @IBOutlet weak var button1: ShadowButton!
    @IBOutlet weak var button2: ShadowButton!
    @IBOutlet weak var button3: ShadowButton!
    @IBOutlet weak var button4: ShadowButton!
    @IBOutlet weak var button5: ShadowButton!
    
    fileprivate let DELAY: TimeInterval = 0.4
    fileprivate var isClicked: Bool = false
    
    fileprivate var subscribers = [Any]()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        button1.alpha = 0.0
        button2.alpha = 0.0
        button3.alpha = 0.0
        button4.alpha = 0.0
        button5.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.button1.addShowAnimation(delay: 0.1)
        self.button2.addShowAnimation(delay: 0.2)
        self.button3.addShowAnimation(delay: 0.3)
        self.button4.addShowAnimation(delay: 0.4)
        self.button5.addShowAnimation(delay: 0.5)
    }
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    //
    
    fileprivate func getIdentifiers(type: Epiduo) -> [String] {
        switch type {
            case .TERAPIA: return ["111", "112", "113", "114", "115"]
            case .SKUTECZNOSC: return ["121", "122", "113", "124", "125", "126", "127"]
            case .DZIALANIE: return ["131", "125", "133"]
            case .REFERENCJE: return ["141"]
            case .SKROCONA: return ["151"]
        }
    }
    
    fileprivate func showSliderVc(type: Epiduo, hideOpisProduktuButton: Bool, hideBottomButtons: Bool) {
        guard self.getIdentifiers(type: type).count > 0 else { return }
        
        self.performSegue("showSliderVc") { (destVc) in
            (destVc as! SliderGridViewController).ids = self.getIdentifiers(type: type)
            (destVc as! SliderGridViewController).type = Storyboards.EPIDUO
            (destVc as! SliderGridViewController).hideOpisProduktuButton = hideOpisProduktuButton
            (destVc as! SliderGridViewController).hideBottomButtons = hideBottomButtons
        }
    }
    
    //
    
    fileprivate func animate(button: UIButton? = nil, normal: EpiduoNormal, selected: EpiduoSelected, type: Epiduo, hideOpisProduktuButton: Bool = true, hideBottomButtons: Bool = false) {
        guard !self.isClicked else { return }
        
        self.isClicked = true
        
        button?.setImage(UIImage(named: selected.rawValue), for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + DELAY) {
            self.showSliderVc(type: type, hideOpisProduktuButton: hideOpisProduktuButton, hideBottomButtons: hideBottomButtons)
            button?.setImage(UIImage(named: normal.rawValue), for: .normal)
            self.isClicked = false
        }
    }
}

// show vc

extension EpiduoViewController {
    func configureNavigation() {
        subscribers.append(EventSubscriber.subscribe(tag: "showEpiduoTerapiaVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .TERAPIA, hideOpisProduktuButton: true, hideBottomButtons: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showEpiduoSkutecznoscVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .SKUTECZNOSC, hideOpisProduktuButton: true, hideBottomButtons: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showEpiduoDzialanieVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .DZIALANIE, hideOpisProduktuButton: false, hideBottomButtons: false)
        }))
    }
}


// IBActions

extension EpiduoViewController {
    @IBAction func terapiaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .TERAPIA, selected: .TERAPIA, type: .TERAPIA)
    }
    
    @IBAction func skutecznoscButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .SKUTECZNOSC, selected: .SKUTECZNOSC, type: .SKUTECZNOSC)
    }
    
    @IBAction func dzialanieButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .DZIALANIE, selected: .DZIALANIE, type: .DZIALANIE, hideOpisProduktuButton: false)
    }
    
    @IBAction func referencjeButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .REFERENCJE, selected: .REFERENCJE, type: .REFERENCJE, hideBottomButtons: true)
    }
    
    @IBAction func skroconaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .SKROCONA, selected: .SKROCONA, type: .SKROCONA, hideBottomButtons: true)
    }
}

//

enum Epiduo {
    case TERAPIA, SKUTECZNOSC, DZIALANIE, REFERENCJE, SKROCONA
}

enum EpiduoNormal: String {
    case TERAPIA = "epiduo_1_terapia"
    case SKUTECZNOSC = "epiduo_2_skutecznosc"
    case DZIALANIE = "epiduo_3_dzialanie"
    case REFERENCJE = "epiduo_4_referencje"
    case SKROCONA = "epiduo_5_skrocona"
}

enum EpiduoSelected: String {
    case TERAPIA = "epiduo_1_terapia_selected"
    case SKUTECZNOSC = "epiduo_2_skutecznosc_selected"
    case DZIALANIE = "epiduo_3_dzialanie_selected"
    case REFERENCJE = "epiduo_4_referencje_selected"
    case SKROCONA = "epiduo_5_skrocona_selected"
}
