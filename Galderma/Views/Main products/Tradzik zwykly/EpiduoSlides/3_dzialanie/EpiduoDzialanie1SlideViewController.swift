//
//  EpiduoDzialanie1SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 21/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoDzialanie1SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var image5: UIImageView!
    @IBOutlet weak var image6: UIImageView!
    @IBOutlet weak var image7: UIImageView!
    @IBOutlet weak var image8: UIImageView!
    @IBOutlet weak var image9: UIImageView!
    @IBOutlet weak var image10: UIImageView!
    @IBOutlet weak var image11: UIImageView!
    @IBOutlet weak var image12: UIImageView!
    @IBOutlet weak var image13: UIImageView!
    
    fileprivate lazy var images: [UIImageView] = {
        return [image1,image2,image3,image4,image5,image6,image7,image8,image9,image10,image11,image12,image13]
    }()
    
    fileprivate var isFirstButtonClicked: Bool = false
    fileprivate var isSecondButtonClicked: Bool = false
    fileprivate var isThirdButtonClicked: Bool = false
    fileprivate var isFourthButtonClicked: Bool = false
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // hide photos with no animation
        self.images.forEach { (image) in
            image.alpha = 0.0
        }
        
        //
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hidePhoto))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc func hidePhoto() {
        self.images.forEach { (image) in
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                image.alpha = 0.0
            })
        }
    }
    
    //
    
    
    @IBAction func firstButtonClicked(_ sender: UIButton) {
        guard !isFirstButtonClicked else { return }
        
        isFirstButtonClicked = true
        sender.isEnabled = false
        
        let image = UIImage(named: "131b")
        self.backgroundImageView.image = image
    }
    
    
    @IBAction func secondButtonClicked(_ sender: UIButton) {
        guard isFirstButtonClicked else { return }
        guard !isSecondButtonClicked else { return }
        
        isSecondButtonClicked = true
        sender.isEnabled = false
        
        let image = UIImage(named: "131c")
        self.backgroundImageView.image = image
    }
    
    @IBAction func thirdButtonClicked(_ sender: UIButton) {
        guard isSecondButtonClicked else { return }
        guard !isThirdButtonClicked else { return }
        
        isThirdButtonClicked = true
        sender.isEnabled = false
        
        let image = UIImage(named: "131d")
        self.backgroundImageView.image = image
    }
    
    @IBAction func fourthButtonClicked(_ sender: UIButton) {
        guard isThirdButtonClicked else { return }
        
        if !isFourthButtonClicked {
            isFourthButtonClicked = true
            let image = UIImage(named: "131c")
            self.backgroundImageView.image = image
        } else {
            isFourthButtonClicked = false
            let image = UIImage(named: "131d")
            self.backgroundImageView.image = image
        }
    }
    
    
    // IBActions
    
    @IBAction func button1Clicked(_ sender: UIButton) {
        self.showPhoto(image1)
    }
    
    @IBAction func button2Clicked(_ sender: UIButton) {
        self.showPhoto(image2)
    }
    
    @IBAction func button3Clicked(_ sender: UIButton) {
        self.showPhoto(image3)
    }
    
    @IBAction func button4Clicked(_ sender: UIButton) {
        self.showPhoto(image4)
    }
    
    @IBAction func button5Clicked(_ sender: UIButton) {
        self.showPhoto(image5)
    }
    
    @IBAction func button6Clicked(_ sender: UIButton) {
        self.showPhoto(image6)
    }
    
    @IBAction func button7Clicked(_ sender: UIButton) {
        self.showPhoto(image7)
    }
    
    @IBAction func button8Clicked(_ sender: UIButton) {
        self.showPhoto(image8)
    }
    
    @IBAction func button9Clicked(_ sender: UIButton) {
        self.showPhoto(image9)
    }
    
    @IBAction func button10Clicked(_ sender: UIButton) {
        self.showPhoto(image10)
    }
    
    @IBAction func button11Clicked(_ sender: UIButton) {
        self.showPhoto(image11)
    }
    
    @IBAction func button12Clicked(_ sender: UIButton) {
        self.showPhoto(image12)
    }
    
    @IBAction func button13Clicked(_ sender: UIButton) {
        self.showPhoto(image13)
    }
    
    //
    
    fileprivate func showPhoto(_ image: UIImageView) {
        guard image.alpha == 0.0 else {
            // click displayed photo again - hide it
            self.hidePhoto()
            return
        }
        
        // hide all others photo when any button selected
        self.images.forEach { (image) in
            if image.alpha == 1.0 {
                self.hidePhoto()
            }
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
            image.alpha = 1.0
        })
    }
}
