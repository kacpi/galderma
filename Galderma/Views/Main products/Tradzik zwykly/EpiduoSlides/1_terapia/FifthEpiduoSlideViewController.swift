//
//  FifthEpiduoSlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class FifthEpiduoSlideViewController: ViewController {
    
    @IBOutlet weak var pulsatingView: UIView!
    
    fileprivate let pulsatingLayer = CAShapeLayer()
    
    //
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.drawCircle()
    }
    
    //
    
    fileprivate func drawCircle() {
        let circularPath = UIBezierPath(arcCenter: .zero, radius: self.pulsatingView.frame.width/2, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        pulsatingLayer.path = circularPath.cgPath
        pulsatingLayer.strokeColor = UIColor.clear.cgColor
        pulsatingLayer.lineWidth = 15
        pulsatingLayer.fillColor = UIColor(hex: "009684").cgColor
        pulsatingLayer.lineCap = CAShapeLayerLineCap.round
        pulsatingLayer.position = pulsatingView.center
        
        self.view.layer.insertSublayer(pulsatingLayer, above: self.pulsatingView.layer)
        
        self.animatePulsatingLayer()
    }
    
    fileprivate func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.fromValue = 1.0
        animation.toValue = 1.15
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    
}
