//
//  FourthEpiduoSlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 13/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class FourthEpiduoSlideViewController: ViewController {
    
    @IBOutlet weak var astmaImageView: UIImageView!
    @IBOutlet weak var cukrzycaImageView: UIImageView!
    @IBOutlet weak var padaczkaImageView: UIImageView!
    
    fileprivate var isFirstButtonSelected: Bool = false
    fileprivate var isSecondButtonSelected: Bool = false
    fileprivate var isThirdButtonSelected: Bool = false
    
    fileprivate let pulsatingLayer = CAShapeLayer()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideElements()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.hideElements()
    }
    
    //
    
    fileprivate func hideElements() {
        let images = [astmaImageView, cukrzycaImageView, padaczkaImageView]
        
        images.forEach { (image) in
            image?.alpha = 0.0
        }
        
        isFirstButtonSelected = false
        isSecondButtonSelected = false
        isThirdButtonSelected = false
        
        pulsatingLayer.removeAllAnimations()
    }
    
    @IBAction func astmaButtonClicked(_ sender: Any) {
        if !self.isFirstButtonSelected {
            self.isFirstButtonSelected = true
            self.animate(image: astmaImageView)
        } else {
            self.isFirstButtonSelected = false
            self.animate(image: astmaImageView, hide: true)
        }
    }
    
    @IBAction func cukrzycaButtonClicked(_ sender: Any) {
        if !self.isSecondButtonSelected {
            self.isSecondButtonSelected = true
            self.animate(image: cukrzycaImageView)
        } else {
            self.isSecondButtonSelected = false
            self.animate(image: cukrzycaImageView, hide: true)
        }
    }
    
    @IBAction func padaczkaButtonClicked(_ sender: Any) {
        if !self.isThirdButtonSelected {
            self.isThirdButtonSelected = true
            self.animate(image: padaczkaImageView)
        } else {
            self.isThirdButtonSelected = false
            self.animate(image: padaczkaImageView, hide: true)
        }
    }
    
    fileprivate func animate(image: UIImageView, hide: Bool = false) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: [.curveEaseOut, .allowUserInteraction], animations: {
            image.alpha = hide ? 0.0 : 1.0
        })
    }
}
