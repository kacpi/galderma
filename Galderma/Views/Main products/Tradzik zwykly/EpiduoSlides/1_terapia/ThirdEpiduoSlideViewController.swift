//
//  ThirdEpiduoSlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 13/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class ThirdEpiduoSlideViewController: ViewController {
    
    @IBOutlet weak var firstCheckbox: UIImageView!
    @IBOutlet weak var secondCheckbox: UIImageView!
    @IBOutlet weak var thirdCheckbox: UIImageView!
    @IBOutlet weak var fourthCheckbox: UIImageView!
    
    @IBOutlet weak var firstTextLabel: UILabel!
    @IBOutlet weak var secondTextLabel: UILabel!
    @IBOutlet weak var thirdTextLabel: UILabel!
    @IBOutlet weak var fourthTextLabel: UILabel!
    
    fileprivate var isFirstButtonSelected: Bool = false
    fileprivate var isSecondButtonSelected: Bool = false
    fileprivate var isThirdButtonSelected: Bool = false
    fileprivate var isFourthButtonSelected: Bool = false
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideElements()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.hideElements()
    }
    
    fileprivate func hideElements() {
        let checkboxes = [firstCheckbox, secondCheckbox, thirdCheckbox, fourthCheckbox]
        let labels = [firstTextLabel, secondTextLabel, thirdTextLabel, fourthTextLabel]
        
        checkboxes.forEach { (checkbox) in
            checkbox?.image = #imageLiteral(resourceName: "checkbox")
        }
        
        labels.forEach { (label) in
            label?.alpha = 0.0
        }
        
        isFirstButtonSelected = false
        isSecondButtonSelected = false
        isThirdButtonSelected = false
        isFourthButtonSelected = false
    }
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        if !isFirstButtonSelected {
            self.isFirstButtonSelected = true
            self.animate(checkbox: firstCheckbox, label: firstTextLabel)
        } else {
            self.isFirstButtonSelected = false
            self.animate(checkbox: firstCheckbox, label: firstTextLabel, hide: true)
        }
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        if !isSecondButtonSelected {
            self.isSecondButtonSelected = true
            self.animate(checkbox: secondCheckbox, label: secondTextLabel)
        } else {
            self.isSecondButtonSelected = false
            self.animate(checkbox: secondCheckbox, label: secondTextLabel, hide: true)
        }
    }
    
    @IBAction func thirdButtonClicked(_ sender: Any) {
        if !isThirdButtonSelected {
            self.isThirdButtonSelected = true
            self.animate(checkbox: thirdCheckbox, label: thirdTextLabel)
        } else {
            self.isThirdButtonSelected = false
            self.animate(checkbox: thirdCheckbox, label: thirdTextLabel, hide: true)
        }
    }
    
    @IBAction func fourthButtonClicked(_ sender: Any) {
        if !isFourthButtonSelected {
            self.isFourthButtonSelected = true
            self.animate(checkbox: fourthCheckbox, label: fourthTextLabel)
        } else {
            self.isFourthButtonSelected = false
            self.animate(checkbox: fourthCheckbox, label: fourthTextLabel, hide: true)
        }
    }
    
    fileprivate func animate(checkbox: UIImageView, label: UILabel, hide: Bool = false) {
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: [.curveEaseOut, .allowUserInteraction, .transitionCrossDissolve], animations: {
//            checkbox.image = hide ? #imageLiteral(resourceName: "checkbox") : #imageLiteral(resourceName: "checkbox_marked")
            label.alpha = hide ? 0.0 : 1.0
        })
        
        UIView.transition(with: checkbox, duration: 0.2, options: .transitionCrossDissolve, animations: {
            checkbox.image = hide ? #imageLiteral(resourceName: "checkbox") : #imageLiteral(resourceName: "checkbox_marked")
        })
    }
    
}
