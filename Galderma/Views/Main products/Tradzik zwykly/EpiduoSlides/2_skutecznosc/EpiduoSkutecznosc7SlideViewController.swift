//
//  EpiduoSkutecznosc7SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 10/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoSkutecznosc7SlideViewController: ViewController {
    
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var pulsatingTextImageView: UIImageView!
    
    fileprivate let pulsatingLayer = CAShapeLayer()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: self.redView.frame.width/2, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        pulsatingLayer.path = circularPath.cgPath
        pulsatingLayer.strokeColor = UIColor(hex: "452374").cgColor
        pulsatingLayer.lineWidth = 15
        pulsatingLayer.fillColor = UIColor.white.cgColor
        pulsatingLayer.lineCap = CAShapeLayerLineCap.round
        pulsatingLayer.position = redView.center
        view.layer.insertSublayer(pulsatingLayer, at: 1) // 1 - to show text inside view
        
        animatePulsatingLayer()
    }
    
    fileprivate func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.fromValue = 0.89
        animation.toValue = 1.0
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        pulsatingLayer.add(animation, forKey: "pulsing")
        pulsatingTextImageView.layer.add(animation, forKey: "pulsingText")
    }
    
}
