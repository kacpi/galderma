//
//  EpiduoSkutecznosc6SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 14/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoSkutecznosc6SlideViewController: ViewController {
    
    @IBOutlet weak var rumienButton: UIButton!
    @IBOutlet weak var luszczenieButton: UIButton!
    @IBOutlet weak var suchoscButton: UIButton!
    @IBOutlet weak var pieczenieButton: UIButton!
    
    @IBOutlet weak var rumienLabel: UILabel!
    @IBOutlet weak var luszczenieLabel: UILabel!
    @IBOutlet weak var suchoscLabel: UILabel!
    @IBOutlet weak var pieczenieLabel: UILabel!
    
    fileprivate var isRumienSelected: Bool = false
    fileprivate var isLuszczenieSelected: Bool = false
    fileprivate var isSuchoscSelected: Bool = false
    fileprivate var isPieczenieSelected: Bool = false
    
    @IBOutlet weak var redView: UIView!
    
    fileprivate let pulsatingLayer = CAShapeLayer()
    
    //
    
    @IBOutlet weak var pulsatingTextImageView: UIImageView!
    
    @IBOutlet weak var pieczenieFirstView: UIView!
    @IBOutlet weak var pieczenieSecondView: UIView!
    @IBOutlet weak var pieczenieThirdView: UIView!
    @IBOutlet weak var pieczenieFourthView: UIView!
    @IBOutlet weak var pieczenieFifthView: UIView!
    
    fileprivate lazy var pieczenieViews: [UIView] = {
        return [pieczenieFirstView, pieczenieSecondView, pieczenieThirdView, pieczenieFourthView]
    }()
    
    @IBOutlet weak var luszczenieFirstView: UIView!
    @IBOutlet weak var luszczenieSecondView: UIView!
    @IBOutlet weak var luszczenieThirdView: UIView!
    
    fileprivate lazy var luszczenieViews: [UIView] = {
        return [luszczenieFirstView, luszczenieSecondView, luszczenieThirdView]
    }()
    
    @IBOutlet weak var suchoscFirstView: UIView!
    @IBOutlet weak var suchoscSecondView: UIView!
    @IBOutlet weak var suchoscThirdView: UIView!
    @IBOutlet weak var suchoscFourthView: UIView!
    @IBOutlet weak var suchoscFifthView: UIView!
    
    fileprivate lazy var suchoscViews: [UIView] = {
        return [suchoscFirstView, suchoscSecondView, suchoscThirdView, suchoscFourthView, suchoscFifthView]
    }()
    
    @IBOutlet weak var rumienFirstView: UIView!
    @IBOutlet weak var rumienSecondView: UIView!
    @IBOutlet weak var rumienThirdView: UIView!
    
    fileprivate lazy var rumienViews: [UIView] = {
        return [rumienFirstView, rumienSecondView]
    }()
    
    fileprivate lazy var pieczeniePath = { () -> UIBezierPath in
        let path = UIBezierPath()
        path.move(to: pieczenieFirstView.center)
        path.addLine(to: pieczenieSecondView.center)
        path.addLine(to: pieczenieThirdView.center)
        path.addLine(to: pieczenieFourthView.center)
        path.addLine(to: pieczenieFifthView.center)
        return path
    }()
    
    fileprivate lazy var luszczeniePath = { () -> UIBezierPath in
        let path = UIBezierPath()
        path.move(to: luszczenieFirstView.center)
        path.addLine(to: luszczenieSecondView.center)
        path.addLine(to: luszczenieThirdView.center)
        return path
    }()
    
    fileprivate lazy var suchoscPath = { () -> UIBezierPath in
        let path = UIBezierPath()
        path.move(to: suchoscFirstView.center)
        path.addLine(to: suchoscSecondView.center)
        path.addLine(to: suchoscThirdView.center)
        path.addLine(to: suchoscFourthView.center)
        path.addLine(to: suchoscFifthView.center)
        return path
    }()
    
    fileprivate lazy var rumienPath = { () -> UIBezierPath in
        let path = UIBezierPath()
        path.move(to: rumienFirstView.center)
        path.addLine(to: rumienSecondView.center)
        path.addLine(to: rumienThirdView.center)
        return path
    }()
    
    fileprivate var rumienLayer = CAShapeLayer()
    fileprivate var luszczenieLayer = CAShapeLayer()
    fileprivate var suchoscLayer = CAShapeLayer()
    fileprivate var pieczenieLayer = CAShapeLayer()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideLabels()
        hideAllViews()
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: self.redView.frame.width/2, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        pulsatingLayer.path = circularPath.cgPath
        pulsatingLayer.strokeColor = UIColor(hex: "452374").cgColor
        pulsatingLayer.lineWidth = 15
        pulsatingLayer.fillColor = UIColor.white.cgColor
        pulsatingLayer.lineCap = CAShapeLayerLineCap.round
        pulsatingLayer.position = redView.center
        view.layer.insertSublayer(pulsatingLayer, at: 1) // 1 - to show text inside view
        
        animatePulsatingLayer()
        
        //
        
        rumienButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        luszczenieButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        suchoscButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        pieczenieButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    }
    
    // IBActions
    
    @IBAction func rumienButtonClicked(_ sender: Any) {
        if !isRumienSelected {
            isRumienSelected = true
            rumienButton.setImage(#imageLiteral(resourceName: "rumienButton"), for: .normal)
            rumienLabel.alpha = 1.0
            
            rumienLayer = getLineLayer(path: rumienPath, color: UIColor(hex: "e1282f"))
            view.layer.addSublayer(rumienLayer)
            
            showViews(rumienViews)
        } else {
            isRumienSelected = false
            rumienButton.setImage(#imageLiteral(resourceName: "rumienPytajnikButton"), for: .normal)
            rumienLabel.alpha = 0.0
            removeLayer(layer: rumienLayer)
            
            hideViews(rumienViews)
        }
    }
    
    @IBAction func luszczenieButtonClicked(_ sender: Any) {
        if !isLuszczenieSelected {
            isLuszczenieSelected = true
            luszczenieButton.setImage(#imageLiteral(resourceName: "luszczenieButton"), for: .normal)
            luszczenieLabel.alpha = 1.0
            
            luszczenieLayer = getLineLayer(path: luszczeniePath, color: UIColor(hex: "a94b8a"))
            view.layer.addSublayer(luszczenieLayer)
            
            showViews(luszczenieViews)
        } else {
            isLuszczenieSelected = false
            luszczenieButton.setImage(#imageLiteral(resourceName: "luszczeniePytajnikButton"), for: .normal)
            luszczenieLabel.alpha = 0.0
            removeLayer(layer: luszczenieLayer)
            
            hideViews(luszczenieViews)
        }
    }
    
    @IBAction func suchoscButtonClicked(_ sender: Any) {
        if !isSuchoscSelected {
            isSuchoscSelected = true
            suchoscButton.setImage(#imageLiteral(resourceName: "suchoscButton"), for: .normal)
            suchoscLabel.alpha = 1.0
            
            suchoscLayer = getLineLayer(path: suchoscPath, color: UIColor(hex: "546162"))
            view.layer.addSublayer(suchoscLayer)
            
            showViews(suchoscViews)
        } else {
            isSuchoscSelected = false
            suchoscButton.setImage(#imageLiteral(resourceName: "suchoscPytajnikButton"), for: .normal)
            suchoscLabel.alpha = 0.0
            removeLayer(layer: suchoscLayer)
            
            hideViews(suchoscViews)
        }
    }
    
    @IBAction func pieczenieButtonClicked(_ sender: Any) {
        if !isPieczenieSelected {
            isPieczenieSelected = true
            pieczenieButton.setImage(#imageLiteral(resourceName: "pieczenieButton"), for: .normal)
            pieczenieLabel.alpha = 1.0
            
            pieczenieLayer = getLineLayer(path: pieczeniePath, color: UIColor(hex: "eeb44a"))
            view.layer.addSublayer(pieczenieLayer)
            
            showViews(pieczenieViews)
        } else {
            isPieczenieSelected = false
            pieczenieButton.setImage(#imageLiteral(resourceName: "pieczeniePytajnikButton"), for: .normal)
            pieczenieLabel.alpha = 0.0
            removeLayer(layer: pieczenieLayer)
            
            hideViews(pieczenieViews)
        }
    }
    
    //
    
    fileprivate func hideLabels() {
        let labels = [rumienLabel, luszczenieLabel, suchoscLabel, pieczenieLabel]
        
        labels.forEach { (label) in
            label?.alpha = 0.0
        }
    }
    
    fileprivate func showViews(_ views: [UIView]) {
        views.forEach { (view_) in
            view_.superview?.bringSubviewToFront(view_)
            view_.alpha = 1.0
        }
    }
    
    fileprivate func hideViews(_ views: [UIView]) {
        views.forEach { (view_) in
            view_.alpha = 0.0
        }
    }
    
    fileprivate func hideAllViews() {
        self.rumienViews.forEach { (view_) in
            view_.alpha = 0.0
        }
        self.luszczenieViews.forEach { (view_) in
            view_.alpha = 0.0
        }
        self.suchoscViews.forEach { (view_) in
            view_.alpha = 0.0
        }
        self.pieczenieViews.forEach { (view_) in
            view_.alpha = 0.0
        }
    }
    
    fileprivate func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.fromValue = 0.89
        animation.toValue = 1.0
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        pulsatingLayer.add(animation, forKey: "pulsing")
        pulsatingTextImageView.layer.add(animation, forKey: "pulsingText")
    }
    
    fileprivate func getLineLayer(path: UIBezierPath, color: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.strokeEnd = 0
        layer.lineWidth = 3
        layer.strokeColor = color.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1.0
        animation.duration = 1.5
        animation.autoreverses = false
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        layer.add(animation, forKey: "")
        
        return layer
    }
    
    fileprivate func removeLayer(layer: CAShapeLayer) {
        self.view.layer.sublayers?.forEach({ (sublayer) in
            if sublayer == layer {
                sublayer.removeFromSuperlayer()
            }
        })
    }
}
