//
//  EpiduoSkutecznosc4SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 13/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoSkutecznosc4SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    fileprivate var isFirstButtonSelected: Bool = false
    fileprivate var isSecondButtonSelected: Bool = false
    
    //
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        isFirstButtonSelected = false
        isSecondButtonSelected = false
        
        self.backgroundImageView.image = #imageLiteral(resourceName: "124")
    }
    
    //
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        if !isFirstButtonSelected && !isSecondButtonSelected {
            isFirstButtonSelected = true
            self.backgroundImageView.image = #imageLiteral(resourceName: "124b")
        } else if !isFirstButtonSelected && isSecondButtonSelected {
            isFirstButtonSelected = true
            self.backgroundImageView.image = #imageLiteral(resourceName: "124d")
        } else if isFirstButtonSelected && isSecondButtonSelected {
            isFirstButtonSelected = false
            self.backgroundImageView.image = #imageLiteral(resourceName: "124c")
        } else {
            isFirstButtonSelected = false
            self.backgroundImageView.image = #imageLiteral(resourceName: "124")
        }
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        if !isSecondButtonSelected && !isFirstButtonSelected {
            isSecondButtonSelected = true
            self.backgroundImageView.image = #imageLiteral(resourceName: "124c")
        } else if !isSecondButtonSelected && isFirstButtonSelected {
            isSecondButtonSelected = true
            self.backgroundImageView.image = #imageLiteral(resourceName: "124d")
        } else if isSecondButtonSelected && !isFirstButtonSelected {
            isSecondButtonSelected = false
            self.backgroundImageView.image = #imageLiteral(resourceName: "124")
        } else {
            isSecondButtonSelected = false
            self.backgroundImageView.image = #imageLiteral(resourceName: "124b")
        }
    }
}
