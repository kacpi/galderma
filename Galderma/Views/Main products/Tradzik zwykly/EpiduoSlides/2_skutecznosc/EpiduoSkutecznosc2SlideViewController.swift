//
//  EpiduoSkutecznosc2SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 13/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoSkutecznosc2SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    fileprivate var isFirstButtonSelected: Bool = false
    fileprivate var isSecondButtonSelected: Bool = false
    fileprivate var isThirdButtonSelected: Bool = false
    
    //
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        if !isFirstButtonSelected {
            isFirstButtonSelected = true
            self.firstButton.setImage(#imageLiteral(resourceName: "skutecznosc"), for: .normal)
        } else {
            isFirstButtonSelected = false
            self.firstButton.setImage(nil, for: .normal)
        }
        
        changeBackground()
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        if !isSecondButtonSelected {
            isSecondButtonSelected = true
            self.secondButton.setImage(#imageLiteral(resourceName: "skutecznosc2"), for: .normal)
        } else {
            isSecondButtonSelected = false
            self.secondButton.setImage(nil, for: .normal)
        }
        
        changeBackground()
    }
    
    @IBAction func thirdButtonClicked(_ sender: Any) {
        if !isThirdButtonSelected {
            isThirdButtonSelected = true
            self.thirdButton.setImage(#imageLiteral(resourceName: "skutecznosc3"), for: .normal)
        } else {
            isThirdButtonSelected = false
            self.thirdButton.setImage(nil, for: .normal)
        }
        
        changeBackground()
    }
    
    fileprivate func changeBackground() {
        if !self.isFirstButtonSelected || !self.isSecondButtonSelected || !self.isThirdButtonSelected {
            self.backgroundImageView.image = #imageLiteral(resourceName: "122")
        } else {
            if self.isFirstButtonSelected && self.isSecondButtonSelected && self.isThirdButtonSelected {
                self.backgroundImageView.image = #imageLiteral(resourceName: "122b")
            }
        }
    }
}
