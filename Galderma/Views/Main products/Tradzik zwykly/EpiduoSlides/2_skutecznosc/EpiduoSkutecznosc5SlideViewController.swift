//
//  EpiduoSkutecznosc5SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 14/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoSkutecznosc5SlideViewController: ViewController {
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    fileprivate var isButtonSelected: Bool = false
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.secondView.alpha = 0.0
        self.thirdView.alpha = 0.0
    }
    
    //
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        if !self.isButtonSelected {
            self.isButtonSelected = true
            self.backgroundImageView.image = #imageLiteral(resourceName: "125b")
        } else {
            self.isButtonSelected = false
            self.backgroundImageView.image = #imageLiteral(resourceName: "125")
        }
    }
    
    @IBAction func smallButtonClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: [], animations: {
            self.secondView.alpha = 1.0
            self.thirdView.alpha = 1.0
        }) { (finished) in
            if finished {
                self.drawLine()
            }
        }
    }
    
    fileprivate func drawLine() {
        let path = UIBezierPath()
        path.move(to: firstView.center)
        path.addLine(to: secondView.center)
        path.addLine(to: thirdView.center)
        path.addLine(to: fourthView.center)
        
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.strokeEnd = 0
        layer.lineWidth = 5
        layer.strokeColor = UIColor(hex: "00928F").cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        
        self.view.layer.addSublayer(layer)
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1.0
        animation.duration = 1.5
        animation.autoreverses = false
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        layer.add(animation, forKey: "line")
    }
    
}
