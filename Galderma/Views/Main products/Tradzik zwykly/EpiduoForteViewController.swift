//
//  EpiduoForteViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoForteViewController: ViewController {
    
    @IBOutlet weak var button1: ShadowButton!
    @IBOutlet weak var button2: ShadowButton!
    @IBOutlet weak var button3: ShadowButton!
    @IBOutlet weak var button4: ShadowButton!
    @IBOutlet weak var button5: ShadowButton!
    
    //
    
    private let DELAY: TimeInterval = 0.4
    private var isClicked: Bool = false
    
    fileprivate var subscribers = [Any]()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.button1.alpha = 0.0
        self.button2.alpha = 0.0
        self.button3.alpha = 0.0
        self.button4.alpha = 0.0
        self.button5.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.button1.addShowAnimation(delay: 0.1)
        self.button2.addShowAnimation(delay: 0.2)
        self.button3.addShowAnimation(delay: 0.3)
        self.button4.addShowAnimation(delay: 0.4)
        self.button5.addShowAnimation(delay: 0.5)
    }
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    //

    fileprivate func getIdentifiers(type: EpiduoForte) -> [String] {
        switch type {
        case .MIEJSCOWE: return ["211", "212", "213", "225", "214"]
        case .IZO: return ["221", "222", "223", "224", "225", "214"]
        case .IGA: return ["231"]
        case .REFERENCJE: return ["241"]
        case .SKROCONA: return ["251"]
        }
    }
    
    fileprivate func showSliderVc(type: EpiduoForte, hideOpisProduktuButton: Bool, hideBottomButtons: Bool, isIgaVc: Bool) {
        guard self.getIdentifiers(type: type).count > 0 else { return }
        
        self.performSegue("showSliderVc") { (destVc) in
            (destVc as! SliderGridViewController).ids = self.getIdentifiers(type: type)
            (destVc as! SliderGridViewController).type = Storyboards.EPIDUO_FORTE
            (destVc as! SliderGridViewController).hideOpisProduktuButton = hideOpisProduktuButton
            (destVc as! SliderGridViewController).hideBottomButtons = hideBottomButtons
            (destVc as! SliderGridViewController).isIgaVc = isIgaVc
        }
    }
    
    //
    
    fileprivate func animate(button: UIButton, normal: EpiduoForteNormal, selected: EpiduoForteSelected, type: EpiduoForte, hideOpisProduktuButton: Bool = true, hideBottomButtons: Bool = false, isIgaVc: Bool = false) {
        guard !self.isClicked else { return }
        
        self.isClicked = true
        
        button.setImage(UIImage(named: selected.rawValue), for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + DELAY) {
            self.showSliderVc(type: type, hideOpisProduktuButton: hideOpisProduktuButton, hideBottomButtons: hideBottomButtons, isIgaVc: isIgaVc)
            button.setImage(UIImage(named: normal.rawValue), for: .normal)
            self.isClicked = false
        }
    }
}

// show vc

extension EpiduoForteViewController {
    func configureNavigation() {
        subscribers.append(EventSubscriber.subscribe(tag: "showEpiduoForteLekiVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .MIEJSCOWE, hideOpisProduktuButton: true, hideBottomButtons: false, isIgaVc: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showEpiduoForteIzoVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .IZO, hideOpisProduktuButton: true, hideBottomButtons: false, isIgaVc: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showEpiduoForteSkalaIgaVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .IGA, hideOpisProduktuButton: true, hideBottomButtons: true, isIgaVc: true)
        }))
    }
}

// IBActions

extension EpiduoForteViewController {
    @IBAction func miejscoweButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .MIEJSCOWE, selected: .MIEJSCOWE, type: .MIEJSCOWE)
    }
    
    @IBAction func izoButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .IZO, selected: .IZO, type: .IZO)
    }
    
    @IBAction func igaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .IGA, selected: .IGA, type: .IGA, hideBottomButtons: true, isIgaVc: true)
    }
    
    @IBAction func referencjeButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .REFERENCJE, selected: .REFERENCJE, type: .REFERENCJE, hideBottomButtons: true)
    }
    
    @IBAction func skroconaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .SKROCONA, selected: .SKROCONA, type: .SKROCONA, hideBottomButtons: true)
    }
}

//

enum EpiduoForte {
    case MIEJSCOWE, IZO, IGA, REFERENCJE, SKROCONA
}

enum EpiduoForteNormal: String {
    case MIEJSCOWE = "epiduo_forte_1_leki"
    case IZO = "epiduo_forte_2_izo"
    case IGA = "epiduo_forte_3_skala"
    case REFERENCJE = "epiduo_forte_4_referencje"
    case SKROCONA = "epiduo_forte_5_skrocona"
}

enum EpiduoForteSelected: String {
    case MIEJSCOWE = "epiduo_forte_1_leki_selected"
    case IZO = "epiduo_forte_2_izo_selected"
    case IGA = "epiduo_forte_3_skala_selected"
    case REFERENCJE = "epiduo_forte_4_referencje_selected"
    case SKROCONA = "epiduo_forte_5_skrocona_selected"
}
