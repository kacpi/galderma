//
//  EpiduoForteSkroconaSlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 16/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit
import WebKit

class EpiduoForteSkroconaSlideViewController: ViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pdfWebView: WKWebView!
    
    override func viewDidLoad() {
        self.activityIndicator.isHidden = false
        self.pdfWebView.navigationDelegate = self
        
        if let pdf = Bundle.main.url(forResource: "pdf_epiduo_forte_opis", withExtension: "pdf") {
            let request = NSURLRequest(url: pdf)
            self.pdfWebView.load(request as URLRequest)
        }
    }
}

extension EpiduoForteSkroconaSlideViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        self.activityIndicator.isHidden = true
    }
}
