//
//  EpiduoForteLeki4SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 19/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoForteLeki4SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var photoImageView: UIImageView!
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hidePhoto))
        gesture.cancelsTouchesInView = false
        self.photoImageView.addGestureRecognizer(gesture)
        
        self.photoImageView.isHidden = true
    }
    
    @objc func hidePhoto() {
        self.photoImageView.image = nil
        self.backgroundImageView.image = UIImage(named: "214")
        self.photoImageView.isHidden = true
    }
    
    @IBAction func firstPhotoButtonClicked(_ sender: Any) {
        self.backgroundImageView.image = UIImage(named: "214b")
        if let image_ = UIImage(named: "214_1.png") {
            self.photoImageView.image = image_
            self.photoImageView.isHidden = false
        }
    }
    
    @IBAction func secondPhotoButtonClicked(_ sender: Any) {
        self.backgroundImageView.image = UIImage(named: "214b")
        if let image_ = UIImage(named: "214_2.png") {
            self.photoImageView.image = image_
            self.photoImageView.isHidden = false
        }
    }
    
    @IBAction func thirdPhotoButtonClicked(_ sender: Any) {
        self.backgroundImageView.image = UIImage(named: "214b")
        if let image_ = UIImage(named: "214_3.png") {
            self.photoImageView.image = image_
            self.photoImageView.isHidden = false
        }
    }
    
    @IBAction func fourthPhotoButtonClicked(_ sender: Any) {
        self.backgroundImageView.image = UIImage(named: "214b")
        if let image_ = UIImage(named: "214_4.png") {
            self.photoImageView.image = image_
            self.photoImageView.isHidden = false
        }
    }
}
