//
//  EpiduoForteLeki2SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 19/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoForteLeki2SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var firstSlupek: UIImageView!
    @IBOutlet weak var secondSlupek: UIImageView!
    @IBOutlet weak var pulsatingView: UIView!
    
    fileprivate var firstPath = UIBezierPath()
    fileprivate var firstMaskLayer = CAShapeLayer()
    
    fileprivate var secondPath = UIBezierPath()
    fileprivate var secondMaskLayer = CAShapeLayer()
    
    fileprivate var isButtonClicked: Bool = false
    
    //
    
    override func viewDidLoad() {
        self.setupMasks()
    }
    
    fileprivate func setupMasks() {
        self.firstPath = UIBezierPath(rect: self.firstSlupek.bounds)
        self.firstPath.append(UIBezierPath(rect: self.firstSlupek.bounds))
        
        self.firstMaskLayer = CAShapeLayer()
        self.firstMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.firstMaskLayer.path = self.firstPath.cgPath
        self.firstSlupek.layer.mask = self.firstMaskLayer
        
        self.secondPath = UIBezierPath(rect: self.secondSlupek.bounds)
        self.secondPath.append(UIBezierPath(rect: self.secondSlupek.bounds))
        
        self.secondMaskLayer = CAShapeLayer()
        self.secondMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.secondMaskLayer.path = self.secondPath.cgPath
        self.secondSlupek.layer.mask = self.secondMaskLayer
    }
    
    //
    
    @IBAction func igaButtonClicked(_ sender: Any) {
        EventSubscriber.publish(tag: "handleTopBlackView_", extraData: true) // hide top black
        EventSubscriber.publish(tag: "handleBottomBlackView_", extraData: true) // hide bottom black
        
        EventSubscriber.publish(tag: "goToEpiduoForteSkalaIgaVc")
    }
    
    @IBAction func wykresButtonClicked(_ sender: Any) {
        guard !isButtonClicked else {
            isButtonClicked = false
            
            let image = UIImage(named: "212") // z pytajnikiem
            self.backgroundImageView.image = image
            
            self.setupMasks()
            
            self.pulsatingView.layer.removeAllAnimations()
            
            return
        }
        
        isButtonClicked = true
        
        let image = UIImage(named: "212b") // bez pytajnika
        self.backgroundImageView.image = image
        
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            if self.isButtonClicked {
                self.animatePulsatingView()
            }
        })
        self.setupAnimation(view: self.firstSlupek, mask: self.firstMaskLayer)
        self.setupAnimation(view: self.secondSlupek, mask: self.secondMaskLayer, delay: 0.5)
        CATransaction.commit()
    }
    
    fileprivate func setupAnimation(view: UIView, mask: CAShapeLayer, delay: Double = 0.0) {
        let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: view.bounds.width, height: 1))
        path.append(UIBezierPath(rect: view.bounds))
        
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = mask.path
        animation.toValue = path.cgPath
        animation.duration = 1.5
        animation.beginTime = CACurrentMediaTime() + delay
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        mask.add(animation, forKey: animation.keyPath)
        
        
        // update the path property on the mask layer, using a CATransaction to prevent an implicit animation
        //        CATransaction.begin()
        //        CATransaction.setDisableActions(true)
        //        mask.path = path.cgPath
        //        CATransaction.commit()
    }
    
    fileprivate func animatePulsatingView() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.fromValue = 1.0
        animation.toValue = 1.15
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        self.pulsatingView.layer.add(animation, forKey: "pulsing")
    }
    
}
