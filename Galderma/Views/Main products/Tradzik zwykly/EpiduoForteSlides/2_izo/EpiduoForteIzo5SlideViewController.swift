//
//  EpiduoForteIzo5SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 20/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoForteIzo5SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    fileprivate var isFirstButtonClicked: Bool = false
    fileprivate var isSecondButtonClicked: Bool = false
    
    //
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        if !isFirstButtonClicked {
            let image = UIImage(named: "225b.png")
            self.backgroundImageView.image = image
            isFirstButtonClicked = true
        } else {
            let image = UIImage(named: "225.png")
            self.backgroundImageView.image = image
            isFirstButtonClicked = false
            isSecondButtonClicked = false
        }
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        guard isFirstButtonClicked else { return }
        
        if !isSecondButtonClicked {
            let image = UIImage(named: "225c.png")
            self.backgroundImageView.image = image
            isSecondButtonClicked = true
        } else {
            let image = UIImage(named: "225b.png")
            self.backgroundImageView.image = image
            isSecondButtonClicked = false
        }
    }
    
}
