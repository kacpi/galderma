//
//  EpiduoForteIzo4SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 19/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoForteIzo4SlideViewController: ViewController {
    
    var image = UIImage()
    
    fileprivate var photoVc: ImageViewController?
    
    //
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let photoVc_ = self.photoVc {
            photoVc_.view.removeFromSuperview()
        }
    }
    
    //
    
    @IBAction func firstPhotoButtonClicked(_ sender: Any) {
        if let image_ = UIImage(named: "224_1.png") {
            self.image = image_
            self.showPhoto()
        }
    }
    
    @IBAction func secondPhotoButtonClicked(_ sender: Any) {
        if let image_ = UIImage(named: "224_2.png") {
            self.image = image_
            self.showPhoto()
        }
    }
    
    @IBAction func thirdPhotoButtonClicked(_ sender: Any) {
        if let image_ = UIImage(named: "224_3.png") {
            self.image = image_
            self.showPhoto()
        }
    }
    
    @IBAction func fourthPhotoButtonClicked(_ sender: Any) {
        if let image_ = UIImage(named: "224_4.png") {
            self.image = image_
            self.showPhoto()
        }
    }
    
    //
    
    fileprivate func showPhoto() {
        let storyboard = UIStoryboard(name: "EpiduoForte", bundle: nil)
        
        if let imageVc = storyboard.instantiateViewController(withIdentifier: "ImageVc") as? ImageViewController {
            
            self.photoVc = imageVc
            
            imageVc.image = self.image
            
            imageVc.didClose = {
                imageVc.view.removeFromSuperview()
                return
            }
            
            imageVc.view.frame = self.view.frame
            self.view.addSubview(imageVc.view)
            
            self.addChild(imageVc)
        }
    }
    
}
