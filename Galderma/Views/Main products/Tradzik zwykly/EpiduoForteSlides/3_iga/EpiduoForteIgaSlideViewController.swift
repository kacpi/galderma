//
//  EpiduoForteIgaSlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 20/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class EpiduoForteIgaSlideViewController: ViewController {
    
    @IBOutlet weak var iga0bottomCnst: NSLayoutConstraint!
    @IBOutlet weak var iga1BottomCnst: NSLayoutConstraint!
    @IBOutlet weak var iga2BottomCnst: NSLayoutConstraint!
    @IBOutlet weak var iga3BottomCnst: NSLayoutConstraint!
    @IBOutlet weak var iga4BottomCnst: NSLayoutConstraint!
    @IBOutlet weak var iga5BottomCnst: NSLayoutConstraint!
    
    @IBOutlet weak var iga0more: UIImageView!
    @IBOutlet weak var iga1more: UIImageView!
    @IBOutlet weak var iga2more: UIImageView!
    @IBOutlet weak var iga3more: UIImageView!
    @IBOutlet weak var iga4more: UIImageView!
    @IBOutlet weak var iga5more: UIImageView!
    
    fileprivate lazy var opisy: [UIImageView] = {
        return [iga0more, iga1more, iga2more, iga3more, iga4more, iga5more]
    }()
    
    fileprivate var isIga0Clicked: Bool = false
    fileprivate var isIga1Clicked: Bool = false
    fileprivate var isIga2Clicked: Bool = false
    fileprivate var isIga3Clicked: Bool = false
    fileprivate var isIga4Clicked: Bool = false
    fileprivate var isIga5Clicked: Bool = false
    
    //
    
    override func viewDidLoad() {
        self.opisy.forEach { (opis) in
            opis.alpha = 0.0
        }
    }
    
    @IBAction func iga0Clicked(_ sender: UIButton) {
        self.iga0bottomCnst.constant = !isIga0Clicked ? 140 : 70
        self.animateMore(view: iga0more, isVisible: !isIga0Clicked)
        self.isIga0Clicked.toggle()
        self.animateConstraints()
    }
    
    @IBAction func iga1Clicked(_ sender: UIButton) {
        self.iga1BottomCnst.constant = !isIga1Clicked ? 140 : 70
        self.animateMore(view: iga1more, isVisible: !isIga1Clicked)
        self.isIga1Clicked.toggle()
        self.animateConstraints()
    }
    
    @IBAction func iga2Clicked(_ sender: UIButton) {
        self.iga2BottomCnst.constant = !isIga2Clicked ? 140 : 70
        self.animateMore(view: iga2more, isVisible: !isIga2Clicked)
        self.isIga2Clicked.toggle()
        self.animateConstraints()
    }
    
    @IBAction func iga3Clicked(_ sender: UIButton) {
        self.iga3BottomCnst.constant = !isIga3Clicked ? 140 : 70
        self.animateMore(view: iga3more, isVisible: !isIga3Clicked)
        self.isIga3Clicked.toggle()
        self.animateConstraints()
    }
    
    @IBAction func iga4Clicked(_ sender: UIButton) {
        self.iga4BottomCnst.constant = !isIga4Clicked ? 140 : 70
        self.animateMore(view: iga4more, isVisible: !isIga4Clicked)
        self.isIga4Clicked.toggle()
        self.animateConstraints()
    }
    
    @IBAction func iga5Clicked(_ sender: UIButton) {
        self.iga5BottomCnst.constant = !isIga5Clicked ? 140 : 70
        self.animateMore(view: iga5more, isVisible: !isIga5Clicked)
        self.isIga5Clicked.toggle()
        self.animateConstraints()
    }
    
    //
    
    fileprivate func animateConstraints() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [.curveEaseOut, .allowUserInteraction], animations: {
            self.view.layoutIfNeeded() //need to call when constraint has changed
        }, completion: nil)
    }
    
    fileprivate func animateMore(view: UIImageView, isVisible: Bool) {
        UIView.animate(withDuration: 0.5, delay: isVisible ? 0.2 : 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [.curveEaseOut, .allowUserInteraction], animations: {
            view.alpha = isVisible ? 1.0 : 0.0
        }, completion: nil)
    }
}
