//
//  SoolantraViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SoolantraViewController: ViewController {
    
    @IBOutlet weak var button1: ShadowButton!
    @IBOutlet weak var button2: ShadowButton!
    @IBOutlet weak var button3: ShadowButton!
    @IBOutlet weak var button4: ShadowButton!
    @IBOutlet weak var button5: ShadowButton!
    @IBOutlet weak var button6: ShadowButton!
    @IBOutlet weak var button7: ShadowButton!
    
    //
    
    private let DELAY: TimeInterval = 0.4
    private var isClicked: Bool = false
    
    fileprivate var subscribers = [Any]()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.button1.alpha = 0.0
        self.button2.alpha = 0.0
        self.button3.alpha = 0.0
        self.button4.alpha = 0.0
        self.button5.alpha = 0.0
        self.button6.alpha = 0.0
        self.button7.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.button1.addShowAnimation(delay: 0.1)
        self.button2.addShowAnimation(delay: 0.2)
        self.button3.addShowAnimation(delay: 0.3)
        self.button4.addShowAnimation(delay: 0.4)
        self.button5.addShowAnimation(delay: 0.5)
        self.button6.addShowAnimation(delay: 0.6)
        self.button7.addShowAnimation(delay: 0.7)
    }
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    //
    
    fileprivate func getIdentifiers(type: Soolantra) -> [String] {
        switch type {
        case .ZASADNICZA: return ["311", "312", "313", "314", "315"]
        case .DLUGOTERMINOWA: return ["321", "322", "323", "324", "325", "326", "327"]
        case .REMISJA: return ["331", "332", "333", "334", "335", "336", "337", "338"]
        case .MECHANIZM: return ["341", "342", "343", "344", "345", "346"]
        case .IGA: return ["351"]
        case .REFERENCJE: return ["361"]
        case .SKROCONA: return ["371"]
        }
    }
    
    fileprivate func showSliderVc(type: Soolantra, hideOpisProduktuButton: Bool = false, hideBottomButtons: Bool, isIgaVc: Bool) {
        guard self.getIdentifiers(type: type).count > 0 else { return }
        
        self.performSegue("showSliderVc") { (destVc) in
            (destVc as! SliderGridViewController).ids = self.getIdentifiers(type: type)
            (destVc as! SliderGridViewController).type = Storyboards.SOOLANTRA
            (destVc as! SliderGridViewController).hideOpisProduktuButton = hideOpisProduktuButton
            (destVc as! SliderGridViewController).hideBottomButtons = hideBottomButtons
            (destVc as! SliderGridViewController).isIgaVc = isIgaVc
        }
    }
    
    //
    
    fileprivate func animate(button: UIButton, normal: SoolantraNormal, selected: SoolantraSelected, type: Soolantra, hideOpisProduktuButton: Bool = false, hideBottomButtons: Bool = false, isIgaVc: Bool = false) {
        guard !self.isClicked else { return }
        
        self.isClicked = true
        
        button.setImage(UIImage(named: selected.rawValue), for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + DELAY) {
            self.showSliderVc(type: type, hideOpisProduktuButton: hideOpisProduktuButton, hideBottomButtons: hideBottomButtons, isIgaVc: isIgaVc)
            button.setImage(UIImage(named: normal.rawValue), for: .normal)
            self.isClicked = false
        }
    }
}

// show vc

extension SoolantraViewController {
    func configureNavigation() {
        subscribers.append(EventSubscriber.subscribe(tag: "showSoolantraZasadniczaVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .ZASADNICZA, hideBottomButtons: false, isIgaVc: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showSoolantraDlugoterminowaVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .DLUGOTERMINOWA, hideBottomButtons: false, isIgaVc: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showSoolantraRemisjaVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .REMISJA, hideBottomButtons: false, isIgaVc: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showSoolantraMechanizmVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .MECHANIZM, hideBottomButtons: false, isIgaVc: false)
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showSoolantraSkalaIgaVc", callback: { [weak self] (_) in
            self?.showSliderVc(type: .IGA, hideBottomButtons: true, isIgaVc: true)
        }))
    }
}

// IBActions

extension SoolantraViewController {
    @IBAction func zasadniczaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .ZASADNICZA, selected: .ZASADNICZA, type: .ZASADNICZA)
    }
    
    @IBAction func dlugoterminowaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .DLUGOTERMINOWA, selected: .DLUGOTERMINOWA, type: .DLUGOTERMINOWA)
    }
    
    @IBAction func remisjaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .REMISJA, selected: .REMISJA, type: .REMISJA)
    }
    
    @IBAction func mechanizmButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .MECHANIZM, selected: .MECHANIZM, type: .MECHANIZM)
    }
    
    @IBAction func igaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .IGA, selected: .IGA, type: .IGA, hideBottomButtons: true, isIgaVc: true)
    }
    
    @IBAction func referencjeButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .REFERENCJE, selected: .REFERENCJE, type: .REFERENCJE, hideBottomButtons: true)
    }
    
    @IBAction func skroconaButtonClicked(_ sender: UIButton) {
        self.animate(button: sender, normal: .SKROCONA, selected: .SKROCONA, type: .SKROCONA, hideBottomButtons: true)
    }
}

//

enum Soolantra {
    case ZASADNICZA, DLUGOTERMINOWA, REMISJA, MECHANIZM, IGA, REFERENCJE, SKROCONA
}

enum SoolantraNormal: String {
    case ZASADNICZA = "soolantra_1_terapia"
    case DLUGOTERMINOWA = "soolantra_2_terapia"
    case REMISJA = "soolantra_3_remisja"
    case MECHANIZM = "soolantra_4_mechanizm"
    case IGA = "soolantra_5_skala"
    case REFERENCJE = "soolantra_6_referencje"
    case SKROCONA = "soolantra_7_skrocona"
}

enum SoolantraSelected: String {
    case ZASADNICZA = "soolantra_1_terapia_selected"
    case DLUGOTERMINOWA = "soolantra_2_terapia_selected"
    case REMISJA = "soolantra_3_remisja_selected"
    case MECHANIZM = "soolantra_4_mechanizm_selected"
    case IGA = "soolantra_5_skala_selected"
    case REFERENCJE = "soolantra_6_referencje_selected"
    case SKROCONA = "soolantra_7_skrocona_selected"
}
