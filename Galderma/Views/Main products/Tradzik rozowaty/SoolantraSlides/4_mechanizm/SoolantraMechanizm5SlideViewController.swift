//
//  SoolantraMechanizm5SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 23/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SoolantraMechanizm5SlideViewController: ViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var firstSlupek: UIImageView!
    fileprivate var firstPath = UIBezierPath()
    fileprivate var firstMaskLayer = CAShapeLayer()
    
    @IBOutlet weak var secondSlupek: UIImageView!
    fileprivate var secondPath = UIBezierPath()
    fileprivate var secondMaskLayer = CAShapeLayer()
    
    fileprivate var isButtonClicked: Bool = false
    
    var image = UIImage()
    
    fileprivate var photoVc: ImageViewController?
    
    //
    
    override func viewDidLoad() {
        self.setupMasks()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let photoVc_ = self.photoVc {
            photoVc_.view.removeFromSuperview()
        }
    }
    
    fileprivate func setupMasks() {
        self.firstPath = UIBezierPath(rect: self.firstSlupek.bounds)
        self.firstPath.append(UIBezierPath(rect: self.firstSlupek.bounds))
        
        self.firstMaskLayer = CAShapeLayer()
        self.firstMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.firstMaskLayer.path = self.firstPath.cgPath
        self.firstSlupek.layer.mask = self.firstMaskLayer
        
        self.secondPath = UIBezierPath(rect: self.secondSlupek.bounds)
        self.secondPath.append(UIBezierPath(rect: self.secondSlupek.bounds))
        
        self.secondMaskLayer = CAShapeLayer()
        self.secondMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.secondMaskLayer.path = self.secondPath.cgPath
        self.secondSlupek.layer.mask = self.secondMaskLayer
    }
    
    //
    
    @IBAction func firstPhotoButtonClicked(_ sender: Any) {
        if let image_ = UIImage(named: "345_1.png") {
            self.image = image_
            self.showPhoto()
        }
    }
    
    @IBAction func secondPhotoButtonClicked(_ sender: Any) {
        if let image_ = UIImage(named: "345_2.png") {
            self.image = image_
            self.showPhoto()
        }
    }
    
    //
    
    fileprivate func showPhoto() {
        let storyboard = UIStoryboard(name: "Soolantra", bundle: nil)
        
        if let imageVc = storyboard.instantiateViewController(withIdentifier: "ImageVc") as? ImageViewController {
            
            self.photoVc = imageVc
            
            imageVc.image = self.image
            
            imageVc.didClose = {
                imageVc.view.removeFromSuperview()
                return
            }
            
            imageVc.view.frame = self.view.frame
            self.view.addSubview(imageVc.view)
            
            self.addChild(imageVc)
        }
    }
    
    //
    
    @IBAction func wykresButtonClicked(_ sender: Any) {
        guard !isButtonClicked else {
            isButtonClicked = false
            
            let image = UIImage(named: "345") // z pytajnikiem
            self.backgroundImageView.image = image
            
            self.setupMasks()
            
            return
        }
        
        isButtonClicked = true
        
        let image = UIImage(named: "345b") // bez pytajnika
        self.backgroundImageView.image = image
        
        self.setupAnimation(view: self.firstSlupek, mask: self.firstMaskLayer)
        self.setupAnimation(view: self.secondSlupek, mask: self.secondMaskLayer, delay: 0.5)
    }
    
    fileprivate func setupAnimation(view: UIView, mask: CAShapeLayer, delay: Double = 0.0) {
        let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: view.bounds.width, height: 1))
        path.append(UIBezierPath(rect: view.bounds))
        
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = mask.path
        animation.toValue = path.cgPath
        animation.duration = 1.5
        animation.beginTime = CACurrentMediaTime() + delay
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        mask.add(animation, forKey: animation.keyPath)
        
        // update the path property on the mask layer, using a CATransaction to prevent an implicit animation
        //        CATransaction.begin()
        //        CATransaction.setDisableActions(true)
        //        mask.path = path.cgPath
        //        CATransaction.commit()
    }
}
