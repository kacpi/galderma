//
//  SoolantraZasadnicza5SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 20/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SoolantraZasadnicza5SlideViewController: ViewController {
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    @IBOutlet weak var fifthView: UIView!
    @IBOutlet weak var sixthView: UIView!
    
    fileprivate var layer = CAShapeLayer()
    
    //
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.drawLine()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.layer.path = nil
    }
    
    fileprivate func drawLine() {
        let path = UIBezierPath()
        path.move(to: firstView.center)
        path.addLine(to: secondView.center)
        path.addLine(to: thirdView.center)
        path.addLine(to: fourthView.center)
        path.addLine(to: fifthView.center)
        path.addLine(to: sixthView.center)
        
        layer.path = path.cgPath
        layer.strokeEnd = 0
        layer.lineWidth = 2
        layer.strokeColor = UIColor(hex: "ED7A23").cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        
        self.view.layer.addSublayer(layer)
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1.0
        animation.duration = 1.5
        animation.autoreverses = false
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        layer.add(animation, forKey: "line")
    }
}
