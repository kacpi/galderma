//
//  SoolantraRemisja5SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 26/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SoolantraRemisja5SlideViewController: ViewController {

    @IBOutlet weak var firstSlupekEmpty: UIImageView! {
        didSet {
            self.firstSlupekEmpty.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
        }
    }
    
    @IBOutlet weak var firstSlupek: UIImageView! {
        didSet {
            self.firstSlupek.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
        }
    }
    
    @IBOutlet weak var iga0Pytajnik: UIImageView!
    @IBOutlet weak var iga0Wynik: UIImageView!
    @IBOutlet weak var iga1Pytajnik: UIImageView!
    @IBOutlet weak var iga1Wynik: UIImageView!
    
    
    fileprivate var firstPath = UIBezierPath()
    fileprivate var firstMaskLayer = CAShapeLayer()
    
    //
    
    @IBOutlet weak var secondSlupekEmpty: UIImageView! {
        didSet {
            self.secondSlupekEmpty.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
        }
    }
    
    @IBOutlet weak var secondSlupek: UIImageView! {
        didSet {
            self.secondSlupek.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
        }
    }
    
    fileprivate var secondPath = UIBezierPath()
    fileprivate var secondMaskLayer = CAShapeLayer()
    
    fileprivate var isFirstButtonClicked: Bool = false
    fileprivate var isSecondButtonClicked: Bool = false
    
    fileprivate var firstNewPath: CGPath!
    fileprivate var firstOldMaskPath: CGPath!
    fileprivate var secondNewPath: CGPath!
    fileprivate var secondOldMaskPath: CGPath!
    
    fileprivate var isFirstAnimationCompleted: Bool = true
    fileprivate var isSecondAnimationCompleted: Bool = true
    
    //
    
    override func viewDidLoad() {
        self.setupMasks()
        
        self.iga0Pytajnik.alpha = 1.0
        self.iga1Pytajnik.alpha = 1.0
        self.iga0Wynik.alpha = 0.0
        self.iga1Wynik.alpha = 0.0
    }
    
    fileprivate func setupMasks() {
        self.firstPath = UIBezierPath(rect: self.firstSlupek.bounds)
        self.firstPath.append(UIBezierPath(rect: self.firstSlupek.bounds))
        
        self.firstMaskLayer = CAShapeLayer()
        self.firstMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.firstMaskLayer.path = self.firstPath.cgPath
        self.firstSlupek.layer.mask = self.firstMaskLayer
        
        self.secondPath = UIBezierPath(rect: self.secondSlupek.bounds)
        self.secondPath.append(UIBezierPath(rect: self.secondSlupek.bounds))
        
        self.secondMaskLayer = CAShapeLayer()
        self.secondMaskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.secondMaskLayer.path = self.secondPath.cgPath
        self.secondSlupek.layer.mask = self.secondMaskLayer
    }
    
    //
    
    @IBAction func buttonClicked(_ sender: Any) {
        guard isFirstAnimationCompleted else { return }
        
        guard !isFirstButtonClicked else {
            isFirstButtonClicked = false
            
            self.iga0Pytajnik.alpha = 1.0
            self.iga0Wynik.alpha = 0.0
            
            self.setupBackAnimation(fromPath: self.firstNewPath, toPath: self.firstOldMaskPath, mask: self.firstMaskLayer)
            
            return
        }
        
        isFirstButtonClicked = true
        
        self.iga0Pytajnik.alpha = 0.0
        self.iga0Wynik.alpha = 1.0
        
        CATransaction.begin()
        self.isFirstAnimationCompleted = false
        CATransaction.setCompletionBlock({
            if self.isFirstButtonClicked {
                self.isFirstAnimationCompleted = true
            }
        })
        self.setupAnimation(view: self.firstSlupek, mask: self.firstMaskLayer, isFirst: true)
        CATransaction.commit()
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        guard isSecondAnimationCompleted else { return }
        
        guard !isSecondButtonClicked else {
            isSecondButtonClicked = false
            
            self.iga1Pytajnik.alpha = 1.0
            self.iga1Wynik.alpha = 0.0
            
            self.setupBackAnimation(fromPath: self.secondNewPath, toPath: self.secondOldMaskPath, mask: self.secondMaskLayer)
            
            return
        }
        
        isSecondButtonClicked = true
        
        self.iga1Pytajnik.alpha = 0.0
        self.iga1Wynik.alpha = 1.0
        
        CATransaction.begin()
        self.isSecondAnimationCompleted = false
        CATransaction.setCompletionBlock({
            if self.isSecondButtonClicked {
                self.isSecondAnimationCompleted = true
            }
        })
        self.setupAnimation(view: self.secondSlupek, mask: self.secondMaskLayer, isFirst: false)
        CATransaction.commit()
    }
    
    fileprivate func setupAnimation(view: UIView, mask: CAShapeLayer, delay: Double = 0.0, isFirst: Bool) {
        let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: view.bounds.width, height: 0.1))
        path.append(UIBezierPath(rect: view.bounds))
        
        if isFirst {
            self.firstOldMaskPath = mask.path
            self.firstNewPath = path.cgPath
        } else {
            self.secondOldMaskPath = mask.path
            self.secondNewPath = path.cgPath
        }
        
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = mask.path
        animation.toValue = path.cgPath
        animation.duration = 1.5
        animation.beginTime = CACurrentMediaTime() + delay
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        mask.add(animation, forKey: animation.keyPath)
        
        // update the path property on the mask layer, using a CATransaction to prevent an implicit animation
        //        CATransaction.begin()
        //        CATransaction.setDisableActions(true)
        //        mask.path = path.cgPath
        //        CATransaction.commit()
    }
    
    fileprivate func setupBackAnimation(fromPath: CGPath, toPath: CGPath, mask: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = fromPath
        animation.toValue = toPath
        animation.duration = 1.5
        animation.beginTime = CACurrentMediaTime()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        mask.add(animation, forKey: animation.keyPath)
    }
    
}
