//
//  SoolantraDlugoterminowa5SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 21/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SoolantraDlugoterminowa5SlideViewController: ViewController {
    
    @IBOutlet weak var pulsatingTextImageView: UIImageView!
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animatePulsatingLayer()
    }
    
    fileprivate func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.fromValue = 0.89
        animation.toValue = 1.35
        animation.duration = 0.7
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        pulsatingTextImageView.layer.add(animation, forKey: "pulsingText")
    }
    
}
