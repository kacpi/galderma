//
//  SoolantraDlugoterminowa2SlideViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 21/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SoolantraDlugoterminowa2SlideViewController: ViewController {
    
    var image = UIImage()
    
    fileprivate var photoVc: ImageViewController?
    
    //
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let photoVc_ = self.photoVc {
            photoVc_.view.removeFromSuperview()
        }
    }
    
    // IBActions
    
    @IBAction func firstButtonClicked(_ sender: UIButton) {
        if let image_ = UIImage(named: "322b.png") {
            self.image = image_
            self.showPhotoVc()
        }
    }
    
    //
    
    fileprivate func showPhotoVc() {
        let storyboard = UIStoryboard(name: "Soolantra", bundle: nil)
        
        if let imageVc = storyboard.instantiateViewController(withIdentifier: "ImageVc") as? ImageViewController {
            
            self.photoVc = imageVc
            
            imageVc.image = self.image
            
            imageVc.didClose = {
                imageVc.view.removeFromSuperview()
                return
            }
            
            imageVc.view.frame = self.view.frame
            self.view.addSubview(imageVc.view)
            
            self.addChild(imageVc)
        }
    }
}
