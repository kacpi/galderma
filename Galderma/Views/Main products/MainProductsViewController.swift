//
//  MainProductsViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 13/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class MainProductsViewController: ViewController {
    
    @IBOutlet weak var tradzikZwyklyButton: ShadowButton!
    @IBOutlet weak var tradzikRozowatyButton: ShadowButton!
    @IBOutlet weak var chplButton: ShadowButton!
    
    //
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        EventSubscriber.publish(tag: "hideBackButton")
        
        self.tradzikZwyklyButton.setImage(#imageLiteral(resourceName: "main_tradzik_zwykly"), for: .normal)
        self.tradzikRozowatyButton.setImage(#imageLiteral(resourceName: "main_tradzik_rozowaty"), for: .normal)
        self.chplButton.setImage(#imageLiteral(resourceName: "main_chpl"), for: .normal)
        
        self.tradzikZwyklyButton.alpha = 0.0
        self.tradzikRozowatyButton.alpha = 0.0
        self.chplButton.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tradzikZwyklyButton.addShowAnimation(delay: 0.1)
        self.tradzikRozowatyButton.addShowAnimation(delay: 0.2)
        self.chplButton.addShowAnimation(delay: 0.3)
    }
    
    //

    @IBAction func tradzikZwyklyButtonClicked(_ sender: Any) {
        UIView.transition(with: self.tradzikZwyklyButton, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.tradzikZwyklyButton.setImage(#imageLiteral(resourceName: "main_tradzik_zwykly_selected"), for: .normal)
        }) { (finished) in
            if finished {
                self.performSegue(withIdentifier: "showTradzik", sender: self)
                EventSubscriber.publish(tag: "showBackButton")
            }
        }
    }
    
    @IBAction func tradzikRozowatyButtonClicked(_ sender: Any) {
        UIView.transition(with: self.tradzikRozowatyButton, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.tradzikRozowatyButton.setImage(#imageLiteral(resourceName: "main_tradzik_rozowaty_selected"), for: .normal)
        }) { (finished) in
            if finished {
                self.performSegue(withIdentifier: "showRozowaty", sender: self)
                EventSubscriber.publish(tag: "showBackButton")
            }
        }
    }
    
    @IBAction func ChplButtonClicked(_ sender: Any) {
        UIView.transition(with: self.chplButton, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.chplButton.setImage(#imageLiteral(resourceName: "main_chpl_selected"), for: .normal)
        }) { (finished) in
            if finished {
                self.performSegue(withIdentifier: "showChpl", sender: self)
                EventSubscriber.publish(tag: "showBackButton")
            }
        }
    }

}
