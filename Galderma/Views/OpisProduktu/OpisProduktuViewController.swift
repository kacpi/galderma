//
//  OpisProduktuViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 15/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class OpisProduktuViewController: ViewController {
    
    @IBOutlet weak var produktImageView: UIImageView!
    
    var productImage: UIImage?
    
    fileprivate var subscribers = [Any]()
    
    //
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let productImage_ = productImage {
            self.produktImageView.image = productImage_
        }
        
        subscribers.append(EventSubscriber.subscribe(tag: "hideOpisProduktuVc", callback: { [weak self] (_) in
            self?.dismiss(animated: false, completion: nil)
        }))
    }
    
    @IBAction func zamknijButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showMenuButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "showMenu", sender: self)
    }
    
}
