//
//  PhotoViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 19/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class PhotoViewController: ViewController {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var image: UIImage?
    
    var didClose: (()->())?
    
    //
    
    override func viewDidLoad() {
        if let image_ = self.image {
            self.photoImageView.image = image_
        }
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideView)))
    }
    
    @objc func hideView() {
        self.didClose?()
    }
}
