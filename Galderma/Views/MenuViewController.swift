//
//  MenuViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 06/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    fileprivate var isClosedViaMenuButton: Bool = false
    
    fileprivate lazy var mainStoryboard = {
        return UIStoryboard(name: "Main", bundle: nil)
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !self.isClosedViaMenuButton {
            EventSubscriber.publish(tag: "hideOpisProduktuVc")
        }
    }
    
    //
    
    @IBAction func tradzikZwyklyButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToTradzikVc")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func epiduoButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoVc")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func epiduoTerapiaButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoTerapiaVc")
        
        self.dismiss()
    }
    
    @IBAction func epiduoSkutecznoscButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoSkutecznoscVc")
        
        self.dismiss()
    }
    
    @IBAction func epiduoDzialanieButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoDzialanieVc")
        
        self.dismiss()
    }
    
    @IBAction func epiduoForteButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoForteVc")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func epiduoForteLekiButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoForteLekiVc")
        
        self.dismiss()
    }
    
    @IBAction func epiduoForteIzoButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoForteIzoVc")
        
        self.dismiss()
    }
    
    @IBAction func epiduoForteSkalaIgaButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToEpiduoForteSkalaIgaVc")
        
        self.dismiss()
    }
    
    @IBAction func tradzikRozowatyButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToTradzikRozowatyVc")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func soolantraButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToSoolantraVc")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func soolantraDlugoterminowaButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToSoolantraDlugoterminowaVc")
        
        self.dismiss()
    }
    
    @IBAction func soolantraZasadniczaButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToSoolantraZasadniczaVc")
        
        self.dismiss()
    }
    
    @IBAction func soolantraRemisjaButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToSoolantraRemisjaVc")
        
        self.dismiss()
    }
    
    @IBAction func soolantraMechanizmButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToSoolantraMechanizmVc")
        
        self.dismiss()
    }
    
    @IBAction func soolantraSkalaIgaButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToSoolantraSkalaIgaVc")
        
        self.dismiss()
    }
    
    @IBAction func chplButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToChplVc")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chplEpiduoButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToChplEpiduoVc")
        
        self.dismiss()
    }
    
    @IBAction func chplForteButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToChplForteVc")
        
        self.dismiss()
    }
    
    @IBAction func chplSoolantraButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToChplSoolantraVc")
        
        self.dismiss()
    }
    
    @IBAction func chplTetralysalButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToChplTetralysalVc")
        
        self.dismiss()
    }
    
    @IBAction func chplRozexButtonClicked(_ sender: UIButton) {
        sender.preventRepeatedPresses()
        EventSubscriber.publish(tag: "goToChplRozexVc")
        
        self.dismiss()
    }
    
    //
    
    fileprivate func dismiss(delay_: Double = 0.5) {
        delay(delay_) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// IBActions

extension MenuViewController {
    @IBAction func closeMenuButtonClicked(_ sender: Any) {
        isClosedViaMenuButton = true
        self.dismiss(animated: true, completion: nil)
    }
}
