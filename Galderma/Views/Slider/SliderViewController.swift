//
//  SliderViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SliderViewController: SwipePagesViewController {
    
    @IBOutlet weak var contentView: UIView!
    
    fileprivate var subscribers = [Any]()
    
    var ids: [String] = []
    
    fileprivate lazy var sliderMenuViewController: SliderMenuViewController = {
        let storyboard = UIStoryboard(name: "Slider", bundle: nil)
        
        if let menuSliderVc = storyboard.instantiateViewController(withIdentifier: "SliderMenuVc") as? SliderMenuViewController {
            menuSliderVc.ids = self.ids
            menuSliderVc.slideDidClicked = { [weak self] (row_) in
                self?.handleHideMenu()
                self?.scrollToPage(index: row_)
            }
            return menuSliderVc
        }
        
        return SliderMenuViewController()
    }()
    
    fileprivate let menuWidth: CGFloat = {
        return 140
    }()
    
    fileprivate var isMenuVisible: Bool = false
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configure(mainView: self.contentView)
        
        subscribers.append(EventSubscriber.subscribe(tag: "previousSlideButtonClicked", callback: { [weak self] (index_) in
            if let index = index_ as? Int {
                guard index > 1 else { return }
                
                self?.handleHideMenu()
                self?.scrollToPage(index: index - 2)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "nextSlideButtonClicked", callback: { [weak self] (index_) in
            if let index = index_ as? Int, let ids_ = self?.ids {
                guard index < ids_.count else { return }
                
                self?.handleHideMenu()
                self?.scrollToPage(index: index)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "showSliderMenuButtonClicked", callback: { [weak self] (_) in
            self?.handleMenuSlider()
        }))
        
        //
        
        self.setupSliderMenu()
    }
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    fileprivate func setupSliderMenu() {
        self.sliderMenuViewController.view.frame = CGRect(x: self.view.frame.maxX, y: 0, width: menuWidth, height: self.view.frame.height)
        self.view.addSubview(self.sliderMenuViewController.view)
        
        self.addChild(self.sliderMenuViewController)
    }
    
    fileprivate func handleMenuSlider() {
        if !isMenuVisible {
            self.handleOpenMenu()
        } else {
            self.handleHideMenu()
        }
    }
    
    fileprivate func handleOpenMenu() {
        self.isMenuVisible = true
        self.performAnimations(transform: CGAffineTransform(translationX: -menuWidth, y: 0))
    }
    
    fileprivate func handleHideMenu() {
        self.isMenuVisible = false
        self.performAnimations(transform: .identity)
    }
    
    fileprivate func performAnimations(transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: [.curveEaseOut, .allowUserInteraction], animations: {
            self.sliderMenuViewController.view.transform = transform
        })
    }
}
