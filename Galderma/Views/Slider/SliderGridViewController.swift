//
//  SliderGridViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SliderGridViewController: ViewController {
    
    @IBOutlet weak var opisProduktuButton: UIButton!
    @IBOutlet weak var opisProduktuVerticalCnst: NSLayoutConstraint!
    
    @IBOutlet weak var slideNumberLabel: UILabel!
    @IBOutlet weak var previousSlideButton: UIButton!
    @IBOutlet weak var nextSlideButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var containerViewHeightCnst: NSLayoutConstraint!
    
    var ids: [String] = []
    var type: Storyboards?
    var hideOpisProduktuButton: Bool = true
    var hideBottomButtons: Bool = false
    var isIgaVc: Bool = false
    
    fileprivate var currentPage: Int = 1
    
    fileprivate var isBlackViewsDisplayed: Bool = false
    
    @IBOutlet weak var bottomBlackView: UIView!
    
    fileprivate var subscribers = [Any]()
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSliderText()
        self.setupDoubleTap()
        
        self.opisProduktuButton.isHidden = hideOpisProduktuButton
        self.opisProduktuVerticalCnst.constant = isIgaVc ? 22 : 0
        
        self.bottomView.isHidden = hideBottomButtons
        self.containerViewHeightCnst.constant = isIgaVc ? 580 : 543
        
        self.bottomBlackView.alpha = 0.0
        
        print("KACPI viewDidLoad")
        
        subscribers.append(EventSubscriber.subscribe(tag: "handleBottomBlackView", callback: { [weak self] (isBlackViewsDisplayed) in
            if let isBlackViewsDisplayed_ = isBlackViewsDisplayed as? Bool {
                UIView.animate(withDuration: 0.5, animations: {
                    self?.bottomBlackView.alpha = isBlackViewsDisplayed_ ? 0.0 : 1.0
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "handleBottomBlackView_", callback: { [weak self] (isBlackViewsDisplayed) in
            if let isBlackViewsDisplayed_ = isBlackViewsDisplayed as? Bool {
                self?.bottomBlackView.alpha = isBlackViewsDisplayed_ ? 0.0 : 1.0
            }
        }))
    }
    
    deinit {
        print("KACPI deinit")
        
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var viewControllers: [UIViewController] = []
        
        if let type = self.type {
            let storyboard = UIStoryboard(name: type.rawValue, bundle: nil)
            
            ids.forEach { (id) in
                let vc = storyboard.instantiateViewController(withIdentifier: id)
                viewControllers.append(vc)
            }
            
            if let sliderVc = segue.destination as? SliderViewController, segue.identifier == "embedSegue" {
                sliderVc.swipePagesDelegate = self
                sliderVc.viewControllerArray = viewControllers
                sliderVc.ids = self.ids
            } else if let opisVc = segue.destination as? OpisProduktuViewController, segue.identifier == "showOpisProduktu" {
                switch type {
                case .EPIDUO:
                    opisVc.productImage = #imageLiteral(resourceName: "151")
                case .EPIDUO_FORTE: break
                case .SOOLANTRA:
                    opisVc.productImage = #imageLiteral(resourceName: "371")
                }
            }
        }
    }
    
    //
    
    fileprivate func setupDoubleTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func doubleTapped() {
        EventSubscriber.publish(tag: "handleTopBlackView", extraData: isBlackViewsDisplayed)
        EventSubscriber.publish(tag: "handleBottomBlackView", extraData: isBlackViewsDisplayed)
        
        self.isBlackViewsDisplayed.toggle()
    }
    
    fileprivate func setupSliderText() {
        let sliderCount = self.ids.count
        self.slideNumberLabel.text = "\(currentPage)/\(sliderCount)"
        self.setupSliderButtons()
    }
    
    fileprivate func setupSliderButtons() {
        if self.currentPage == 1 {
            self.previousSlideButton.setImage(#imageLiteral(resourceName: "previousSlideEmpty"), for: .normal)
            self.previousSlideButton.isEnabled = false
        } else {
            self.previousSlideButton.setImage(#imageLiteral(resourceName: "previousSlide"), for: .normal)
            self.previousSlideButton.isEnabled = true
        }
        
        if self.currentPage == self.ids.count {
            self.nextSlideButton.setImage(#imageLiteral(resourceName: "nextSlideEmpty"), for: .normal)
            self.nextSlideButton.isEnabled = false
        } else {
            self.nextSlideButton.setImage(#imageLiteral(resourceName: "nextSlide"), for: .normal)
            self.nextSlideButton.isEnabled = true
        }
    }
    
    @IBAction func previousSlideButtonClicked(_ sender: Any) {
        EventSubscriber.publish(tag: "previousSlideButtonClicked", extraData: currentPage)
    }
    
    @IBAction func nextSlideButtonClicked(_ sender: Any) {
        EventSubscriber.publish(tag: "nextSlideButtonClicked", extraData: currentPage)
    }
    
    @IBAction func showSliderMenuButtonClicked(_ sender: Any) {
        EventSubscriber.publish(tag: "showSliderMenuButtonClicked", extraData: currentPage)
    }
    
    @IBAction func opisProduktuButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "showOpisProduktu", sender: self)
    }
    
}

//

extension SliderGridViewController: SwipePagesDelegate {
    func pageDidShow(index: Int) {
        self.currentPage = index + 1
        self.setupSliderText()
    }
}

//

enum Storyboards: String {
    case EPIDUO = "Epiduo"
    case EPIDUO_FORTE = "EpiduoForte"
    case SOOLANTRA = "Soolantra"
}
