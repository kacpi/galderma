//
//  SliderMenuTableViewCell.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 15/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SliderMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sliderImage: UIImageView!
    
    //

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        // super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupImage(imageName: String) {
        self.sliderImage.image = UIImage(named: imageName)
        
        self.sliderImage.layer.borderWidth = 1.0
        self.sliderImage.layer.borderColor = UIColor(hex: "0092C0").cgColor
        
        self.sliderImage.layer.masksToBounds = true
    }
    
}
