//
//  SliderMenuViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 15/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class SliderMenuViewController: ViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var ids: [String] = []
    
    var slideDidClicked: ((_ row: Int)->())?
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initTableView()
    }
    
    //
}

extension SliderMenuViewController: UITableViewDelegate, UITableViewDataSource {
    fileprivate func initTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "SliderMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SliderMenuTableViewCell")
        
        self.tableView.rowHeight = 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ids.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SliderMenuTableViewCell") as! SliderMenuTableViewCell
        
        cell.setupImage(imageName: self.ids[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.slideDidClicked?(indexPath.row)
    }
    
    
}
