//
//  PdfViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 07/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit
import WebKit

class PdfViewController: ViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var pdfWebView: WKWebView!
    
    @IBOutlet weak var buttonsStackView: UIStackView!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    //
    
    var pdfName: String?
    var logoImage: UIImage?
    var isStackViewVisible: Bool = false
    
    //
    
    override func viewDidLoad() {
        self.activityIndicator.isHidden = false
        self.pdfWebView.navigationDelegate = self
        self.firstButton.isSelected = true
        
        self.setupBorder()
        self.setupButtons()
        
        if let logo = self.logoImage {
            self.logoImageView.image = logo
        }
        
        if let filename = self.pdfName {
            if let pdf = Bundle.main.url(forResource: filename, withExtension: "pdf") {
                let request = NSURLRequest(url: pdf)
                self.pdfWebView.load(request as URLRequest)
            }
        }
    }
    
    fileprivate func setupBorder() {
        self.buttonsStackView.isHidden = !self.isStackViewVisible
        
        let buttons = [self.firstButton, self.secondButton]
        
        buttons.forEach { (button) in
            button?.layer.masksToBounds = true
            button?.layer.borderWidth = 1.0
            button?.layer.borderColor = UIColor.bluePdfButton.cgColor
        }
    }
    
    fileprivate func setupButtons() {
        if self.firstButton.isSelected {
            self.firstButton.backgroundColor = UIColor.clear
            self.firstButton.setTitleColor(UIColor.bluePdfButton, for: .selected)
            self.secondButton.backgroundColor = UIColor.bluePdfButton
            self.secondButton.setTitleColor(UIColor.white, for: .normal)
        } else {
            self.firstButton.backgroundColor = UIColor.bluePdfButton
            self.firstButton.setTitleColor(UIColor.white, for: .normal)
            self.secondButton.backgroundColor = UIColor.clear
            self.secondButton.setTitleColor(UIColor.bluePdfButton, for: .selected)
        }
    }
    
    //
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        self.firstButton.isSelected = true
        self.secondButton.isSelected = false
        
        self.setupButtons()
        
        if let pdf = Bundle.main.url(forResource: "pdf_tetralysal_150", withExtension: "pdf") {
            let request = NSURLRequest(url: pdf)
            self.pdfWebView.load(request as URLRequest)
        }
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        self.firstButton.isSelected = false
        self.secondButton.isSelected = true
        
        self.setupButtons()
        
        if let pdf = Bundle.main.url(forResource: "pdf_tetralysal_300", withExtension: "pdf") {
            let request = NSURLRequest(url: pdf)
            self.pdfWebView.load(request as URLRequest)
        }
    }
}

extension PdfViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        self.activityIndicator.isHidden = true
    }
}
