//
//  ViewController.swift
//  Galderma
//
//  Created by Kacper Piątkowski on 06/11/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    fileprivate var subscribers = [Any]()
    
    @IBOutlet weak var topBlackView: UIView!
    
    //

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigation()
        
        subscribers.append(EventSubscriber.subscribe(tag: "showBackButton", callback: { [weak self] (_) in
            self?.backButton.alpha = 1.0
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "hideBackButton", callback: { [weak self] (_) in
            self?.backButton.alpha = 0.0
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "handleTopBlackView", callback: { [weak self] (isBlackViewsDisplayed) in
            if let isBlackViewsDisplayed_ = isBlackViewsDisplayed as? Bool {
                UIView.animate(withDuration: 0.5, animations: {
                    self?.topBlackView.alpha = isBlackViewsDisplayed_ ? 0.0 : 1.0
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "handleTopBlackView_", callback: { [weak self] (isBlackViewsDisplayed) in
            if let isBlackViewsDisplayed_ = isBlackViewsDisplayed as? Bool {
                self?.topBlackView.alpha = isBlackViewsDisplayed_ ? 0.0 : 1.0
            }
        }))
        
        //
        
        self.topBlackView.alpha = 0.0
    }
    
    deinit {
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
}

// IBActions

extension MainViewController {
    @IBAction func showMenuButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "showMenu", sender: self)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        if let nav = self.children.first as? UINavigationController {
            nav.popViewController(animated: false)
        }
    }
    
    @IBAction func logoButtonClicked(_ sender: Any) {
        if let nav = self.children.first as? UINavigationController {
            nav.popToRootViewController(animated: false)
        }
    }
}

// NAVIGATION

extension MainViewController {
    func configureNavigation() {
        subscribers.append(EventSubscriber.subscribe(tag: "goToTradzikVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc], animated: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoVc], animated: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoTerapiaVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showEpiduoTerapiaVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoSkutecznoscVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showEpiduoSkutecznoscVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoDzialanieVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showEpiduoDzialanieVc")
                })
            }
        }))
        
        
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoForteVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoForteVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoForteVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoForteVc], animated: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoForteLekiVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoForteVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoForteVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoForteVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showEpiduoForteLekiVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoForteIzoVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoForteVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoForteVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoForteVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showEpiduoForteIzoVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToEpiduoForteSkalaIgaVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikZwyklyVc") else { return }
                guard let epiduoForteVc = self?.storyboard?.instantiateViewController(withIdentifier: "epiduoForteVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikVc, epiduoForteVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showEpiduoForteSkalaIgaVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToTradzikRozowatyVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc], animated: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToSoolantraVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                guard let soolantraVc = self?.storyboard?.instantiateViewController(withIdentifier: "soolantraVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc, soolantraVc], animated: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToSoolantraDlugoterminowaVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                guard let soolantraVc = self?.storyboard?.instantiateViewController(withIdentifier: "soolantraVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc, soolantraVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showSoolantraDlugoterminowaVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToSoolantraZasadniczaVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                guard let soolantraVc = self?.storyboard?.instantiateViewController(withIdentifier: "soolantraVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc, soolantraVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showSoolantraZasadniczaVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToSoolantraRemisjaVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                guard let soolantraVc = self?.storyboard?.instantiateViewController(withIdentifier: "soolantraVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc, soolantraVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showSoolantraRemisjaVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToSoolantraMechanizmVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                guard let soolantraVc = self?.storyboard?.instantiateViewController(withIdentifier: "soolantraVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc, soolantraVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showSoolantraMechanizmVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToSoolantraSkalaIgaVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let tradzikRozowatyVc = self?.storyboard?.instantiateViewController(withIdentifier: "tradzikRozowatyVc") else { return }
                guard let soolantraVc = self?.storyboard?.instantiateViewController(withIdentifier: "soolantraVc") else { return }
                
                nav.setViewControllers([mainProducts, tradzikRozowatyVc, soolantraVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showSoolantraSkalaIgaVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToChplVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let chplVc = self?.storyboard?.instantiateViewController(withIdentifier: "chplVc") else { return }
                
                nav.setViewControllers([mainProducts, chplVc], animated: false)
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToChplEpiduoVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let chplVc = self?.storyboard?.instantiateViewController(withIdentifier: "chplVc") else { return }
                
                nav.setViewControllers([mainProducts, chplVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showChplEpiduoVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToChplForteVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let chplVc = self?.storyboard?.instantiateViewController(withIdentifier: "chplVc") else { return }
                
                nav.setViewControllers([mainProducts, chplVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showChplForteVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToChplSoolantraVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let chplVc = self?.storyboard?.instantiateViewController(withIdentifier: "chplVc") else { return }
                
                nav.setViewControllers([mainProducts, chplVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showChplSoolantraVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToChplTetralysalVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let chplVc = self?.storyboard?.instantiateViewController(withIdentifier: "chplVc") else { return }
                
                nav.setViewControllers([mainProducts, chplVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showChplTetralysalVc")
                })
            }
        }))
        
        subscribers.append(EventSubscriber.subscribe(tag: "goToChplRozexVc", callback: { [weak self] (_) in
            EventSubscriber.publish(tag: "showBackButton")
            
            if let nav = self?.children.first as? UINavigationController {
                guard let mainProducts = self?.storyboard?.instantiateViewController(withIdentifier: "mainProductsVc") else { return }
                guard let chplVc = self?.storyboard?.instantiateViewController(withIdentifier: "chplVc") else { return }
                
                nav.setViewControllers([mainProducts, chplVc], animated: false)
                
                delay(1.0, withCompletion: {
                    EventSubscriber.publish(tag: "showChplRozexVc")
                })
            }
        }))
    }
}
